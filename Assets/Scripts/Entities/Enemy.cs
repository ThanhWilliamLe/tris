﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    Vector3 oriRot = Vector3.up;
    Vector3 rotAxis = Vector3.forward;

    [HideInInspector]
    public Spawner spawner;
    [HideInInspector]
    public Vector3 velocity = Vector3.zero;
    [HideInInspector]
    public Transform target;

    [Header("Configurables")]
    public float speed = 1.5f;
    public float speedDampen = 0.8f;
    public float angularSpeed = 60;
    public float power = 1;
    public float health = 1;
    public float score = 1;
    public bool dead = false;
    public bool isBoss = false;

    public float Speed
    {
        get { return speed / GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.EnemySlow); }
    }

    public float AngularSpeed
    {
        get
        {
            return angularSpeed
              * Mathf.Pow(GameManager.CurrentGame.gameDifficulty, 0.25f)
              / GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.EnemySlow);
        }
    }

    public float Power
    {
        get { return power * Mathf.Pow(GameManager.CurrentGame.gameDifficulty, 0.35f); }
    }

    public virtual void OnSpawned()
    {
        target = GameManager.CurrentPlayer.transform;
        RotateTowards(-1, target.position);
        dead = false;
    }

    public void EnemyFixedUpdate(float timePassed)
    {
        UpdateBehavior(timePassed);
    }

    public void EnemyUpdate(float timePassed)
    {
        Move(timePassed);
    }

    protected virtual void UpdateBehavior(float timePassed)
    {
        if (target != null)
        {
            VelocityTowards(timePassed, target.position);
            //RotateTowards(timePassed, target.position);
            RotateTowards(timePassed, transform.position + velocity);
        }
    }

    public void RotateTowards(float timePassed, Vector3 pos, bool instant = false)
    {
        Vector3 toTarget = pos - transform.position;
        float currentAngle = transform.localRotation.eulerAngles.z;
        float diffAngle = Vector3.SignedAngle(transform.rotation * oriRot, toTarget, rotAxis);
        if (timePassed >= 0 && !instant) diffAngle = Mathf.Clamp(diffAngle, -AngularSpeed * timePassed, AngularSpeed * timePassed);
        transform.localRotation = Quaternion.Euler(0, 0, currentAngle + diffAngle);
    }

    public void VelocityTowards(float timePassed, Vector3 pos, bool instant = false)
    {
        Vector3 toTarget = Vector3.ClampMagnitude(pos - transform.position, 1) * Speed;
        if (!instant) velocity += toTarget * timePassed;
        else velocity = toTarget;
        if (velocity.sqrMagnitude > Speed * Speed)
        {
            velocity *= 1 - (1 - speedDampen) * timePassed;
        }
    }

    void Move(float timePassed)
    {
        gameObject.transform.position += velocity * timePassed;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (target != null && col.gameObject == target.gameObject)
        {
            Attack(target.gameObject);
            Die(true);
        }
    }

    public void Damaged(float f, bool addScore = false)
    {
        health -= f;
        SoundManager.Instance.PlaySound(SoundName.Hit_Enemy);
        if (health <= 0)
        {
            Die(addScore);
        }
    }

    public void Attack(GameObject go)
    {
        Player p = go.GetComponent<Player>();
        if (p != null)
        {
            SoundManager.Instance.PlaySound(SoundName.Hit_Player);
            p.HP -= Power;
        }
    }

    public void Die(bool addScore = false)
    {
        dead = true;
        OnDeath();
        if (addScore) GameManager.CurrentPlayer.Score += score;
        Pool();
    }

    public virtual void OnDeath()
    {
        velocity = Vector3.zero;
    }

    void Pool()
    {
        GameManager.CurrentGame.spawnManager.PoolEnemy(this);
    }
}
