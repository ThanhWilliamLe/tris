﻿
using UnityEngine;

public class PowerSpeed : Power
{
    public float playerSpeed = 1.25f;
    public float enemySlow = 2f;
    public float time = 5f;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        bool playerBuff = Random.value < 0.5f;
        if(playerBuff) GameManager.CurrentGame.AddBuff(new Buff(BuffType.PlayerSpeed, playerSpeed, time));
        else GameManager.CurrentGame.AddBuff(new Buff(BuffType.EnemySlow, enemySlow, time));
    }
}