﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public float power = 0;
    public float maxSize = 4;
    public float maxLife = 1;
    public bool push = false;
    public float lifeRatioToDie = 1.2f;

    HashSet<Collider2D> damaged = new HashSet<Collider2D>();
    float life;
    float size;
    float originalAlpha;

    public void SpawnFrom(PowerForceField pff)
    {
        transform.position = pff.transform.position;
        transform.localScale = Vector3.zero;

        spriteRenderer.color = pff.color;
        power = pff.power * GameManager.CurrentPlayer.upgradePower * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.PlayerDamage);
        maxSize = pff.size
            * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.ForceFieldSize)
            * GameData.GetUpgradePowerValue(UpgradePowerType.ForceSize, 1);
        maxLife = pff.time;
        push = pff.push;

        life = 0;
        size = 0;
        originalAlpha = pff.color.a;

        SoundManager.Instance.PlaySound(SoundName.Power_ForceField);
    }

    public void SpawnFrom(BulletMissile missile)
    {
        transform.position = missile.transform.position;
        transform.localScale = Vector3.zero;

        spriteRenderer.color = missile.forceFieldColor;
        power = missile.ForceFieldPower;
        maxSize = missile.forceFieldSize
            * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.ForceFieldSize)
            * GameData.GetUpgradePowerValue(UpgradePowerType.ForceSize, 1);
        maxLife = 0.25f;
        push = false;

        life = 0;
        size = 0;
        originalAlpha = missile.forceFieldColor.a;

        SoundManager.Instance.PlaySound(SoundName.Power_ForceField);
    }

    private void Update()
    {
        size = MathHelpers.PowerLerp(0, maxSize, life / maxLife, 0.5f, false);
        transform.localScale = Vector3.one * size;
        life = life + Time.deltaTime;
        if (life > maxLife)
        {
            float lifeToDie = maxLife * lifeRatioToDie;
            float alpha = originalAlpha * Mathf.Pow(Mathf.InverseLerp(lifeToDie, maxLife, life), 1.5f);
            Color c = spriteRenderer.color;
            c.a = alpha;
            spriteRenderer.color = c;
            if (life > maxLife * lifeRatioToDie) Destroy(gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (life > maxLife) return;
        if (push) Push(collider);
        if (!damaged.Contains(collider)) Damage(collider);
    }

    void Push(Collider2D col)
    {
        Vector3 localEdgePos = (col.transform.position - transform.position).normalized * size / 2f;
        col.transform.position = transform.position + localEdgePos;
    }

    void Damage(Collider2D col)
    {
        Enemy e = col.gameObject.GetComponent<Enemy>();
        if (e != null)
        {
            e.Damaged(power, true);
        }
        damaged.Add(col);
    }
}
