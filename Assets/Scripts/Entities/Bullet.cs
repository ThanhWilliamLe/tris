﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    public BulletType type;
    public LayerMask targetLayer;
    public float maxLife = 1f;
    public float lifeToScale = 0.15f;
    public float baseSpeed = 20f;
    public float basePower = 1f;
    public bool dead = false;

    protected Vector3 oriScale;
    protected Vector3 velocity;
    protected PlayerShooting ps;
    protected Rigidbody2D rb;
    protected float damage;
    protected float life;
    protected float actualSpeed;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Shot(Player player)
    {
        dead = false;
        life = maxLife;
        ps = player.playerShooting;

        Vector3 shootDir = ShootDir(player);

        transform.rotation = player.transform.rotation;
        transform.position = player.transform.position + shootDir * 0.25f;

        actualSpeed = baseSpeed * player.upgradeSpeed * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.PlayerSpeed);
        velocity = shootDir * actualSpeed;
        damage = basePower * player.upgradePower * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.PlayerDamage);
        oriScale = transform.localScale;
    }

    public Vector3 ShootDir(Player player)
    {
        return player.transform.localRotation * Vector3.up;
    }

    public void UpdateBullet(float timePassed)
    {
        if (dead) return;
        if (life <= 0)
        {
            Die();
            return;
        }
        if (life < lifeToScale)
        {
            float ratio = Mathf.Clamp01(life / lifeToScale);
            transform.localScale = oriScale * ratio;
        }

        life -= timePassed;
    }

    public void FixedUpdateBullet(float timePassed)
    {
        if (dead) return;
        BulletFixedBehavior(timePassed);
        BulletMove(timePassed);
    }

    protected virtual void BulletFixedBehavior(float timePassed)
    {

    }

    void BulletMove(float timePassed)
    {
        rb.MovePosition(transform.position + velocity * timePassed);
        transform.rotation = Quaternion.FromToRotation(Vector3.up, velocity);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!dead && ((1 << other.gameObject.layer) & targetLayer.value) != 0) HitTarget(other.gameObject);
    }

    void HitTarget(GameObject go)
    {
        Enemy e = go.GetComponent<Enemy>();
        if (e != null)
        {
            HitEnemy(e);
            return;
        }

        Player p = go.GetComponent<Player>();
        if (p != null)
        {
            HitPlayer(p);
            return;
        }
    }

    protected virtual void HitEnemy(Enemy e)
    {
        if (!e.dead)
        {
            e.Damaged(damage, true);
            Die();
        }
    }

    protected virtual void HitPlayer(Player p)
    {
        if (!p.Dead)
        {
            p.HP -= damage;
            Die();
        }
    }

    public void Die()
    {
        dead = true;
        OnDeath();
        transform.localScale = oriScale;
        ps.PoolBullet(this);
    }

    protected virtual void OnDeath()
    {

    }
}
