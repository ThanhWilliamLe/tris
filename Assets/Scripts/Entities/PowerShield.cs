﻿using UnityEngine;

public class PowerShield : Power
{
    public float time = 5f;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        player.Shield += time * GameData.GetUpgradePowerValue(UpgradePowerType.BuffTime, 1);
    }
}