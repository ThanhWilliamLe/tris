﻿using UnityEngine;

public class PowerMissile : Power
{
    public int ammoCount;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        player.MissileCount += ammoCount;
    }
}