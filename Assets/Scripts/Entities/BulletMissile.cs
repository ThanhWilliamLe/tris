﻿using UnityEngine;
using System.Collections;

public class BulletMissile : Bullet
{
    public float homingRadius = 15;
    public float rotatingSpeed = 90;
    public GameObject forceFieldPrefab;
    public Color forceFieldColor;
    public float forceFieldSize = 5;
    public Transform target;

    public float ForceFieldPower
    {
        get { return damage / 2f; }
    }

    protected override void BulletFixedBehavior(float timePassed)
    {
        base.BulletFixedBehavior(timePassed);

        if (target != null && target.gameObject != null)
        {
            Enemy e = target.gameObject.GetComponent<Enemy>();
            if (e != null && e.dead) target = null;
        }
        if (target != null && target.gameObject != null)
        {
            Player p = target.gameObject.GetComponent<Player>();
            if (p != null && p.Dead) target = null;
        }

        if (target == null)
        {
            if ((targetLayer & (1 << LayerMask.NameToLayer("Player"))) != 0) target = GameManager.CurrentPlayer.transform;
            else target = GameManager.CurrentGame.FindClosest(transform.position, EntityType.Enemy, homingRadius);
        }

        if (target != null)
        {
            Vector3 targetVelocity = Vector3.ClampMagnitude(target.position - transform.position, 1) * actualSpeed;
            float angleToRot = Mathf.Clamp(Vector3.SignedAngle(velocity, targetVelocity, Vector3.forward), -rotatingSpeed, rotatingSpeed);
            float ratio = angleToRot / rotatingSpeed;

            float velocityMag = Mathf.Lerp(targetVelocity.magnitude, velocity.magnitude, ratio);
            velocity = Quaternion.Euler(0, 0, angleToRot * timePassed) * velocity.normalized * velocityMag;
        }
    }

    protected override void OnDeath()
    {
        base.OnDeath();
        GameObject forceField = Instantiate(forceFieldPrefab, GameManager.CurrentGame.playerBulletsGroup);
        ForceField bff = forceField.GetComponent<ForceField>();
        bff.SpawnFrom(this);
    }
}
