﻿

using UnityEngine;

public class EnemyDiver : Enemy
{
    static float THRESHOLD = 0.1f;
    Vector3 targetPos;

    public override void OnSpawned()
    {
        base.OnSpawned();
        GameBounds gb = GameManager.CurrentGame.bounds;
        targetPos = -transform.position;
        RotateTowards(0, targetPos, true);
    }

    protected override void UpdateBehavior(float timePassed)
    {
        RotateTowards(0, targetPos, true);
        VelocityTowards(timePassed, targetPos);
        if ((targetPos - transform.position).sqrMagnitude <= THRESHOLD)
        {
            Die(false);
        }
    }

}