﻿using UnityEngine;

public class PowerHealth : Power
{
    //public Vector2 healRange = new Vector2(0.25f, 0.75f);
    public float heal = 1;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        //player.HP += Mathf.Lerp(healRange.x, healRange.y, Random.value) * player.MaxHP;
        player.HP += heal * GameData.GetUpgradePowerValue(UpgradePowerType.Health, 1);
    }
}