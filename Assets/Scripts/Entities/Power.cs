﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Power : MonoBehaviour
{
    [HideInInspector]
    public Spawner spawner;
    public Vector2 lifeRange = new Vector2(5, 8);
    public Vector2 alphaMinMax = new Vector2(0.4f, 1.5f);
    public float deathAnimTime = 0.25f;
    public bool eaten;

    SpriteRenderer sr;
    float initLife;
    float life;

    public void Spawned()
    {
        eaten = false;
        sr = GetComponent<SpriteRenderer>();
        initLife = life = Mathf.Lerp(lifeRange.x, lifeRange.y, Random.value)
            * GameData.GetUpgradePowerValue(UpgradePowerType.BuffTime, 1);
        PostSpawned();
    }

    public virtual void PostSpawned()
    {

    }

    public void PowerUpdate(float timePassed)
    {
        if (!eaten)
        {
            if (life > 0)
            {
                life -= timePassed;

                Color c = sr.color;
                c.a = Mathf.Lerp(alphaMinMax.x, alphaMinMax.y, life / initLife);
                sr.color = c;
            }
            else StartCoroutine(Destroy(Vector3.zero));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!eaten)
        {
            Eaten();
            eaten = true;
        }
    }

    public void Eaten()
    {
        SoundManager.Instance.PlaySound(SoundName.Power_Eat, 0.5f);
        DoPower(GameManager.CurrentPlayer);
        StartCoroutine(Destroy(transform.localScale * 3, 3f, true));
    }

    public virtual void DoPower(Player player)
    {
    }

    IEnumerator Destroy(Vector3 scaleTo, float spinSpeed = 1f, bool alpha = false)
    {
        float oriAlpha = sr.color.a;
        Vector3 originalScale = transform.localScale;
        float animTime = deathAnimTime;
        while (animTime > 0 && gameObject != null && transform != null)
        {
            yield return new WaitUntil(() => GameManager.CurrentGame.GameState == GameState.Play);
            animTime -= Time.deltaTime;
            transform.Rotate(0, 0, 360 * Time.deltaTime * spinSpeed);
            transform.localScale = Vector3.Lerp(originalScale, scaleTo, 1 - animTime / deathAnimTime);
            if (alpha)
            {
                Color c = sr.color;
                c.a = Mathf.Lerp(oriAlpha, 0, 1 - animTime / deathAnimTime);
                sr.color = c;
            }
        }
        if (gameObject != null) Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (GameManager.CurrentGame != null)
            GameManager.CurrentGame.spawnManager.PowerDestroyed(this);
    }
}
