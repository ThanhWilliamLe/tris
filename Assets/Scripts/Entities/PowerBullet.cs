﻿using UnityEngine;

public class PowerBullet : Power
{
    public int ammoCount;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        player.BulletCount += ammoCount;
    }
}