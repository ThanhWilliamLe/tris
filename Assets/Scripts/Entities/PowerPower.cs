﻿using UnityEngine;

public class PowerPower : Power
{
    public float power = 2f;
    public float time = 5f;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        GameManager.CurrentGame.AddBuff(
            new Buff(BuffType.ForceFieldSize | BuffType.PlayerDamage, power, time), false
            );
    }
}