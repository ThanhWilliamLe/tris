﻿using UnityEngine;

public class EnemyZigZag : Enemy
{
    public float zigZagLength = 1;
    public float zigZagInterval = 2;
    public Vector2 zigZagAngle = new Vector2(15, 30);

    float zigZagDelay = 0;
    float nextZigZagAngle = 15;
    Vector3? zigZagTarget;

    public override void OnSpawned()
    {
        base.OnSpawned();
        zigZagDelay = zigZagInterval;
        nextZigZagAngle = Random.Range(zigZagAngle.x, zigZagAngle.y) * (Random.Range(0, 2) * 2 - 1);
    }

    public override void OnDeath()
    {
        base.OnDeath();
        zigZagTarget = null;
    }

    protected override void UpdateBehavior(float timePassed)
    {
        if (zigZagTarget != null)
        {
            RotateTowards(timePassed, (Vector3)zigZagTarget, true);
            VelocityTowards(timePassed, (Vector3)zigZagTarget, true);
            Vector3 diff = (Vector3)zigZagTarget - transform.position;
            if (diff.sqrMagnitude <= 0.001) zigZagTarget = null;
        }
        else if (target != null)
        {
            Vector3 zigDir = target.position - transform.position;
            if (zigDir.sqrMagnitude > zigZagLength * zigZagLength)
            {
                zigDir = Quaternion.Euler(0, 0, nextZigZagAngle) * zigDir.normalized * zigZagLength;
            }

            RotateTowards(timePassed, transform.position + zigDir);

            zigZagDelay = zigZagDelay - timePassed;
            if (zigZagDelay <= 0)
            {
                zigZagTarget = transform.position + zigDir;
                zigZagDelay = zigZagInterval;
                nextZigZagAngle = Random.Range(zigZagAngle.x, zigZagAngle.y) * Mathf.Sign(nextZigZagAngle) * -1;
            }
        }
    }

    public bool IsBetween(float f, float f0, float f1)
    {
        if (f0 == f1) return f == f0;
        if (f1 > f0) return f1 >= f && f >= f0;
        if (f0 > f1) return f0 >= f && f >= f1;
        return false;
    }
}