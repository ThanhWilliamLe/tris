﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    static Vector3 rotOri = Vector3.up;
    static Vector3 rotAxis = Vector3.forward;

    float _maxhp;
    public float MaxHP
    {
        get { return _maxhp; }
        set
        {
            _maxhp = value;
            GameManager.Instance.InvokeEvent(GameEvent.HPChanged, HP / MaxHP);
        }
    }

    float _hp;
    public float HP
    {
        get { return _hp; }
        set
        {
            if (god && value < _hp) return;

            if (value > HP || value < HP && Shield <= 0)
            {
                _hp = Mathf.Clamp(value, 0, MaxHP);
                GameManager.Instance.InvokeEvent(GameEvent.HPChanged, HP / MaxHP);
            }
            if (value < HP && Shield > 0)
            {
                Shield -= (HP - value);
            }
        }
    }
    public bool Dead
    {
        get { return HP > 0; }
    }

    float _score;
    public float Score
    {
        get { return _score; }
        set
        {
            _score = value;
            GameManager.Instance.InvokeEvent(GameEvent.ScoreChanged, value);
        }
    }

    int _bulletCount;
    int _maxBulletCount;
    public int BulletCount
    {
        get { return _bulletCount; }
        set
        {
            if (value > _bulletCount) _maxBulletCount = value;
            _bulletCount = value;
            //_maxBulletCount = Mathf.Max(_bulletCount, _maxBulletCount);
            GameManager.Instance.InvokeEvent(GameEvent.BulletCountChanged, new int[] { _bulletCount, _maxBulletCount });
        }
    }

    int _missileCount;
    int _maxMissleCount;
    public int MissileCount
    {
        get { return _missileCount; }
        set
        {
            if (value > _missileCount) _maxMissleCount = value;
            _missileCount = value;
            //_maxMissleCount = Mathf.Max(_missileCount, _maxMissleCount);
            GameManager.Instance.InvokeEvent(GameEvent.MissleCountChanged, new int[] { _missileCount, _maxMissleCount });
        }
    }

    float _shield;
    float _maxShield;
    public float Shield
    {
        get { return _shield; }
        set
        {
            if (value > _shield) _maxShield = value;
            _shield = Mathf.Max(0, value);
            ShieldChanged();
            GameManager.Instance.InvokeEvent(GameEvent.ShieldChanged, new float[] { _shield, _maxShield });
        }
    }

    [SerializeField]
    float speedBase = 5;
    public float Speed
    {
        get { return speedBase * upgradeSpeed * GameManager.CurrentGame.GetMultipliedBuffValue(BuffType.PlayerSpeed); }
    }

    public float upgradeSpeed;
    public float upgradePower;

    Rigidbody2D rb;
    public PlayerShooting playerShooting;
    public SpriteRenderer shieldVisual;
    public bool god;

    private void Start()
    {
        MaxHP = 20 + GameData.GetUpgradePowerValue(UpgradePowerType.Health, 0);
        HP = MaxHP;
        Shield = 0;
        Score = 0;
        BulletCount = 0;
        MissileCount = 0;

        upgradeSpeed = 1 * GameData.GetUpgradePowerValue(UpgradePowerType.Speed, 1);
        upgradePower = 1 * GameData.GetUpgradePowerValue(UpgradePowerType.Power, 1);

        rb = GetComponent<Rigidbody2D>();
    }

    public void UpdatePlayer(float timePassed)
    {
        if (Shield > 0) Shield -= timePassed;
    }

    public void FixedUpdatePlayer(float timePassed)
    {

    }

    public void SetMovement(Vector2 dir, float timePassed)
    {
        if (GameManager.CurrentGame.GameState == GameState.End)
        {
            rb.velocity = Vector3.zero;
            return;
        }
        rb.velocity = dir * Speed * GameManager.CurrentGame.timeMult;
        if (dir.x != 0 || dir.y != 0)
        {
            float angle = Vector3.SignedAngle(rotOri, rb.velocity, rotAxis);
            rb.MoveRotation(angle);
        }
    }

    public void ShootBullet()
    {
        if (BulletCount > 0)
        {
            BulletCount--;
            playerShooting.Shoot(this, BulletType.Bullet);
        }
    }

    public void ShootMissile()
    {
        if (MissileCount > 0)
        {
            MissileCount--;
            playerShooting.Shoot(this, BulletType.Missile);
        }
    }

    void ShieldChanged()
    {
        float shieldStartFade = 2.5f;
        float alpha = Mathf.Clamp01(Shield / shieldStartFade);
        Color c = shieldVisual.color;
        c.a = alpha * 0.75f;
        shieldVisual.color = c;
    }

    public int ScoreToCoin(float sc)
    {
        return Mathf.CeilToInt(sc * 0.05f);
    }
}

[System.Serializable]
public class PlayerShooting
{
    public Vector3 poolPos = new Vector3(-100, 100, 0);
    public Transform bulletGroup;

    public GameObject bulletPrefab;
    Queue<GameObject> bulletPool = new Queue<GameObject>();

    public GameObject missilePrefab;
    Queue<GameObject> missilePool = new Queue<GameObject>();

    HashSet<Bullet> spawnedBullets = new HashSet<Bullet>();

    public void Shoot(Player player, BulletType type)
    {
        GameObject prefab = type == BulletType.Bullet ? bulletPrefab : missilePrefab;
        Queue<GameObject> pool = type == BulletType.Bullet ? bulletPool : missilePool;
        SoundManager.Instance.PlaySound(type == BulletType.Bullet ? SoundName.Fire_Bullet : SoundName.Fire_Missile);
        if (pool.Count == 0)
        {
            GameObject newBullet = Object.Instantiate(prefab, bulletGroup);
            Bullet newBulletScript = newBullet.GetComponent<Bullet>();
            PoolBullet(newBulletScript);
        }

        GameObject bullet = pool.Dequeue();
        bullet.transform.position = player.transform.position;
        Bullet bulletScript = bullet.GetComponent<Bullet>();
        bulletScript.Shot(player);
        spawnedBullets.Add(bulletScript);
    }

    public void Update(float timePassed)
    {
        RemoveAllDead();
        foreach (Bullet b in spawnedBullets)
        {
            b.UpdateBullet(timePassed);
        }
    }

    public void FixedUpdate(float timePassed)
    {
        RemoveAllDead();
        foreach (Bullet b in spawnedBullets)
        {
            b.FixedUpdateBullet(timePassed);
        }
    }

    public void RemoveAllDead()
    {
        spawnedBullets.RemoveWhere((b) => b.dead);
    }

    public void PoolBullet(Bullet bullet)
    {
        Queue<GameObject> pool = bullet.type == BulletType.Bullet ? bulletPool : missilePool;
        if (pool != null)
        {
            bullet.dead = true;

            bullet.gameObject.transform.position = poolPos;
            pool.Enqueue(bullet.gameObject);
        }
        else
        {
            Object.Destroy(bullet.gameObject);
        }
    }

}

public enum BulletType
{
    Bullet,
    Missile
}