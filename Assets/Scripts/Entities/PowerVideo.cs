﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class PowerVideo : Power
{
    public override void PostSpawned()
    {
        base.PostSpawned();
        if (!Advertisement.IsReady("rewardedVideo")) Destroy(gameObject);
    }

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        GameManager.CurrentGame.GameState = GameState.Pause;
        GameManager.CurrentGame.gameUIRunner.AskWatchVideo();
    }
}