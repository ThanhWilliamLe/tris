﻿

using System.Collections.Generic;
using UnityEngine;

public class EnemyMazed : Enemy
{
    static float THRESHOLD = 0.1f;
    public Vector2 stepRange = new Vector2(1, 2);

    float maxDist;
    bool mazing = false;
    Rect rect;
    Vector3 nextMazeDir;
    Vector3 targetPos;

    public override void OnSpawned()
    {
        base.OnSpawned();
        mazing = false;
        rect = GameManager.CurrentGame.bounds.Rect;
        maxDist = transform.position.magnitude;
        targetPos = target.position;
    }

    protected override void UpdateBehavior(float timePassed)
    {
        RotateTowards(0, targetPos, true);
        VelocityTowards(timePassed, targetPos, true);

        if (!mazing && MathHelpers.InBound(transform.position, rect.min, rect.max))
        {
            mazing = true;
            targetPos = transform.position;

            nextMazeDir = -transform.position.normalized;
            if (nextMazeDir.x * nextMazeDir.x < 0.5f) nextMazeDir.x = 0;
            else if (nextMazeDir.y * nextMazeDir.y < 0.5f) nextMazeDir.y = 0;
            else if (Random.value < 0.5) nextMazeDir.x = 0;
            else nextMazeDir.y = 0;
            nextMazeDir = nextMazeDir.normalized;
        }

        if (mazing && (targetPos - transform.position).sqrMagnitude < THRESHOLD)
        {
            if (!MathHelpers.InBound(transform.position, rect.min, rect.max))
            {
                Die(false);
                return;
            }
            targetPos = transform.position + nextMazeDir * Random.Range(stepRange.x, stepRange.y);
            if (!MathHelpers.InBound(targetPos, rect.min, rect.max))
            {
                targetPos *= maxDist / targetPos.magnitude;
            }
            nextMazeDir = Quaternion.Euler(0, 0, (Random.Range(0, 2) * 2 - 1) * 90) * nextMazeDir;
        }
    }

}