﻿using UnityEngine;

public class PowerForceField : Power
{
    public GameObject bulletPrefab;
    public Color color = Color.white;
    public float power = 0;
    public float size = 4;
    public float time = 1;
    public bool push = true;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        GameObject bullet = Instantiate(bulletPrefab, GameManager.CurrentGame.playerBulletsGroup);
        ForceField bff = bullet.GetComponent<ForceField>();
        bff.SpawnFrom(this);
    }
}