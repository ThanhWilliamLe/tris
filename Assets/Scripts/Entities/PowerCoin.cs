﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerCoin : Power
{
    public int coin;
    public int maxSpawn = 10;
    public float spawnTime = 0.5f;
    public float spawnRandomness = 1f;

    public override void DoPower(Player player)
    {
        base.DoPower(player);
        GameManager.CurrentGame.AddAndSpawnCoins(transform.position, coin, maxSpawn, spawnRandomness, spawnTime);
    }
}