﻿using UnityEngine;

public class EnemyTwirl : Enemy
{
    static float THRESHOLD = 0.1f;
    public float twirlSpeed = 60;
    [Range(0, 1)]
    public float tendencyRetain = 0.2f;

    Vector3 targetPos;
    Vector3 rotating;
    Vector3 tendency;
    Rect rect;
    float maxDist;
    bool twirling;

    public override void OnSpawned()
    {
        base.OnSpawned();
        twirling = false;
        maxDist = transform.position.magnitude;
        rect = GameManager.CurrentGame.bounds.Rect;
        rect.min += Vector2.one;
        rect.max -= Vector2.one;
        targetPos = target.position;
    }

    protected override void UpdateBehavior(float timePassed)
    {
        if (!twirling)
        {
            if (transform.position.sqrMagnitude > maxDist * maxDist)
            {
                Die(false);
                return;
            }
            RotateTowards(0, targetPos, true);
            VelocityTowards(timePassed, targetPos);
            if (MathHelpers.InBound(transform.position, rect.min, rect.max))
            {
                twirling = true;
                tendency = velocity.normalized;
                rotating = velocity.normalized;
            }
        }
        else
        {
            if (MathHelpers.InBound(transform.position, rect.min, rect.max))
            {
                RotateTowards(0, transform.position + velocity, true);
                rotating = Quaternion.Euler(0, 0, twirlSpeed * timePassed) * rotating;
                velocity = (rotating * (1 - tendencyRetain) + tendency * tendencyRetain) * Speed;
            }
            else
            {
                targetPos = transform.position.normalized * maxDist;
                twirling = false;
            }
        }
    }

}