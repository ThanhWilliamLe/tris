﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WeightedList<T, U> where T : WeightedEntry<U>
{
    public List<T> list;

    public float GetListWeight()
    {
        float f = 0;
        for (int i = 0; i < list.Count; i++) f += list[i].weight;
        return f;
    }

    public U GetRandom()
    {
        float listWeight = GetListWeight();
        float rndWeight = UnityEngine.Random.value * listWeight;
        float countedWeight = 0;
        T w;
        for (int i = 0; i < list.Count; i++)
        {
            w = list[i];
            countedWeight += w.weight;
            if (countedWeight > rndWeight) return w.entry;
        }
        return default(U);
    }
}

[Serializable]
public class WeightedEntry<T>
{
    public T entry;
    public float weight;
}
