﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hotkey : MonoBehaviour
{
    public KeyCode key;
    public UnityEvent onKeyPressed;

    void Update()
    {
        if (Input.GetKeyDown(key)) onKeyPressed.Invoke();
    }
}
