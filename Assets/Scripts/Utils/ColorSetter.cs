﻿using UnityEngine;
using System.Linq;
using System.Reflection;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ColorSetter : MonoBehaviour
{
    public Component target;
    public string colorPath = "color";

    public ColorPalette colorPalette;
    public ColorGroup colorGroup;
    public int colorIndex;

    public bool SetColor()
    {
        if (ReflectionUtils.IsPropertyValue<Color>(target, colorPath, true, true))
        {
            if (colorPalette != null && colorGroup != null && colorIndex >= 0 && colorIndex < colorGroup.colors.Length)
            {
                Color c = colorGroup.colors[colorIndex];
                return ReflectionUtils.SetPropertyValue<Color>(target, colorPath, c);
            }
        }
        return false;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ColorSetter), true)]
[CanEditMultipleObjects]
public class ColorSetterEditor : Editor
{

    public override void OnInspectorGUI()
    {
        ColorSetter t = target as ColorSetter;

        GUILayout.Space(5);
        EditorGUILayout.LabelField("Target", EditorStyles.boldLabel);
        t.target = (Component)EditorGUILayout.ObjectField("Target", t.target, typeof(Component), true);
        t.colorPath = EditorGUILayout.TextField("Target color path", t.colorPath);

        GUILayout.Space(5);
        EditorGUILayout.LabelField("Color", EditorStyles.boldLabel);
        
        t.colorPalette = (ColorPalette)EditorGUILayout.ObjectField("Color palette", t.colorPalette, typeof(ColorPalette), true);

        GUILayout.BeginHorizontal();

        string[] colorGroups = ColorGroups(t.colorPalette);
        int selectedColorGroupID = EditorGUILayout.Popup(SelectedColorGroupID(colorGroups, t.colorGroup), colorGroups);
        t.colorGroup = SelectedColorGroup(t.colorPalette, selectedColorGroupID);

        int maxIndex = 0;
        if (t.colorGroup != null && t.colorGroup.colors != null && t.colorGroup.colors.Length > 0)
            maxIndex = t.colorGroup.colors.Length - 1;
        t.colorIndex = EditorGUILayout.IntSlider(t.colorIndex, 0, maxIndex);

        Color selectedColor = Color.white;
        if (t.colorGroup != null && t.colorGroup.colors != null && t.colorGroup.colors.Length > t.colorIndex)
            selectedColor = t.colorGroup.colors[t.colorIndex];
        EditorGUILayout.ColorField(selectedColor);

        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        if (GUILayout.Button("Set Color")) t.SetColor();
    }

    public string[] ColorGroups(ColorPalette palette)
    {
        if (palette == null) return new string[0];

        List<string> gc = new List<string>();
        for (int i = 0; i < palette.palette.Length; i++)
        {
            if (palette.palette[i] != null && palette.palette[i].name != null) gc.Add(palette.palette[i].name);
        }
        return gc.ToArray();
    }

    public int SelectedColorGroupID(string[] ss, ColorGroup colorGroup)
    {
        if (colorGroup == null || colorGroup.name == null) return -1;

        for (int i0 = 0; i0 < ss.Length; i0++)
        {
            if (ss[i0] != null && colorGroup.name.Equals(ss[i0])) return i0;
        }
        return -1;
    }

    public ColorGroup SelectedColorGroup(ColorPalette palette, int id)
    {
        if (palette == null || id < 0 || id >= palette.palette.Length) return null;
        return palette.palette[id];
    }
}
#endif