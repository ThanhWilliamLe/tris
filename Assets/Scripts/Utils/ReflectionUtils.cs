﻿using System;
using System.Reflection;

public static class ReflectionUtils
{
    public static BindingFlags defaultFlags = BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance;

    public static T GetPropertyValue<T>(object o, string t, object[] index = null)
    {
        Type tt = typeof(T);
        PropertyInfo pi = o.GetType().GetProperty(t, defaultFlags);
        if (pi != null && tt.IsAssignableFrom(pi.PropertyType) && pi.CanRead)
        {
            return (T)pi.GetValue(o, index);
        }
        return default(T);
    }

    public static bool SetPropertyValue<T>(object o, string t, T value, object[] index = null)
    {
        Type tt = typeof(T);
        PropertyInfo pi = o.GetType().GetProperty(t, defaultFlags);
        if (pi != null && tt.IsAssignableFrom(pi.PropertyType) && pi.CanWrite)
        {
            pi.SetValue(o, value, index);
            return true;
        }
        return false;
    }

    public static bool IsPropertyValue<T>(object o, string t, bool mustRead = false, bool mustWrite = false)
    {
        Type tt = typeof(T);
        PropertyInfo pi = o.GetType().GetProperty(t, defaultFlags);
        if (pi != null && tt.IsAssignableFrom(pi.PropertyType))
        {
            if (mustRead && !pi.CanRead) return false;
            if (mustWrite && !pi.CanWrite) return false;
            return true;
        }
        return false;
    }

    public static PropertyInfo GetPropertyInfo<T>(object o, string t, bool mustRead = false, bool mustWrite = false)
    {
        Type tt = typeof(T);
        PropertyInfo pi = o.GetType().GetProperty(t, defaultFlags);
        if (pi != null && tt.IsAssignableFrom(pi.PropertyType))
        {
            if (mustRead && !pi.CanRead) return null;
            if (mustWrite && !pi.CanWrite) return null;
            return pi;
        }
        return null;
    }
}