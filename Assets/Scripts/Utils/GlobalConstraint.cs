﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class GlobalConstraint : MonoBehaviour
{
    public bool active;
    public bool fixPos, fixRot, fixScale;
    public Vector3 fixedPos;
    public Vector3 fixedRot;
    public Vector3 fixedScale;

    void Update()
    {
        if (active)
        {
            if (fixPos)
            {
                transform.position = fixedPos;
            }
            if (fixRot)
            {
                transform.rotation = Quaternion.Euler(fixedRot);
            }
            if (fixScale)
            {
                transform.localScale = fixedScale;
            }
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(GlobalConstraint))]
public class GlobalConstraintEditor : Editor
{
    public override void OnInspectorGUI()
    {
        GlobalConstraint t = target as GlobalConstraint;

        t.active = EditorGUILayout.Toggle("Active", t.active);

        EditorGUILayout.Space();

        t.fixPos = EditorGUILayout.Toggle("Fix position", t.fixPos);
        if (t.fixPos)
        {
            t.fixedPos = EditorGUILayout.Vector3Field("", t.fixedPos);
            if (GUILayout.Button("Set position")) t.fixedPos = t.transform.position;
        }

        EditorGUILayout.Space();

        t.fixRot = EditorGUILayout.Toggle("Fix rotation", t.fixRot);
        if (t.fixRot)
        {
            t.fixedRot = EditorGUILayout.Vector3Field("", t.fixedRot);
            if (GUILayout.Button("Set rotation")) t.fixedRot = t.transform.rotation.eulerAngles;
        }

        EditorGUILayout.Space();

        t.fixScale = EditorGUILayout.Toggle("Fix scale", t.fixScale);
        if (t.fixScale)
        {
            t.fixedScale = EditorGUILayout.Vector3Field("", t.fixedScale);
            if (GUILayout.Button("Set position")) t.fixedScale = t.transform.localScale;
        }
    }
}
#endif