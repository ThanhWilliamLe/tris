﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Tagger : MonoBehaviour
{
    public string objTag;

    public static T FindFirstWithTag<T>(string tag, GameObject parent) where T : Component
    {
        Tagger[] tgs = parent.GetComponentsInChildren<Tagger>();
        Tagger first = FindFirstWithTag(tag, tgs);
        if (first != null) return first.gameObject.GetComponent<T>();
        return null;
    }

    public static T[] FindAllWithTag<T>(string tag, GameObject parent) where T : Component
    {
        Tagger[] tgs = parent.GetComponentsInChildren<Tagger>();
        Tagger[] alls = FindAllWithTag(tag, tgs);
        List<T> ts = new List<T>();
        foreach (Tagger t in alls)
        {
            T comp = t.gameObject.GetComponent<T>();
            if (comp != null) ts.Add(comp);
        }
        return ts.ToArray(); ;
    }

    public static Tagger FindFirstWithTag(string tag, params Tagger[] gos)
    {
        foreach (Tagger t in gos)
        {
            if (t.objTag.Equals(tag)) return t;
        }
        return null;
    }

    public static Tagger[] FindAllWithTag(string tag, params Tagger[] gos)
    {
        List<Tagger> taggers = new List<Tagger>();
        foreach (Tagger t in gos)
        {
            if (t.objTag.Equals(tag)) taggers.Add(t);
        }
        return taggers.ToArray();
    }
}