﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabController : MonoBehaviour
{
    public List<TabButton> buttons;

    public float buttonOriHeight = 50;
    public float buttonPressedHeight = 60;

    public Color buttonOriColor = Color.white;
    public Color buttonPressedColor = Color.white;

    private void Start()
    {
        if (buttons.Count > 0) TabButtonPressed(buttons[0]);
    }

    public void TabButtonPressed(TabButton button)
    {
        foreach (TabButton b in buttons)
        {
            b.content.SetActive(b == button);

            Vector2 v = ((RectTransform)b.transform).sizeDelta;
            v.y = b == button ? buttonPressedHeight : buttonOriHeight;
            ((RectTransform)b.transform).sizeDelta = v;

            b.graphic.color = b == button ? buttonPressedColor : buttonOriColor;
        }
    }
}