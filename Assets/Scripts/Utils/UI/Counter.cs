﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour
{
    public Text text;
    public bool hideObjectOnDone = true;
    public bool hideTextOnDone = false;
    public float time = 1;
    public float delay = 0;
    public float startNumber = 100;
    public float endNumber = 0;
    public string format = "#";

    float timePassed;

    public void StartCounter(float from, float to, float t, float d = -1)
    {
        startNumber = from;
        endNumber = to;
        time = t;
        if (d >= 0) delay = d;
        enabled = true;
    }

    private void OnEnable()
    {
        timePassed = -Mathf.Abs(delay);
    }

    private void Update()
    {
        if (timePassed <= time)
        {
            timePassed += Time.deltaTime;
            if (timePassed >= 0)
            {
                float ratio = Mathf.Clamp01(timePassed / time);
                float number = Mathf.Lerp(startNumber, endNumber, ratio);

                text.text = string.IsNullOrEmpty(format) ? number.ToString() : number.ToString(format);
            }
        }
        else
        {
            if (hideTextOnDone && text.enabled) text.enabled = false;
            if (hideObjectOnDone) gameObject.SetActive(false);
        }
    }
}