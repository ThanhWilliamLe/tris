﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ScaleButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool clickable = true;
    public float scale = 0.8f;
    public float time = 0.1f;
    public float peak = 0.3f;
    public UnityEvent onClick;

    bool shouldUnscale;
    Vector3 originalScale;
    IEnumerator scaling;
    IEnumerator unscaling;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!clickable) return;
        RemoveCoroutines();
        DoScale();
        shouldUnscale = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!clickable) return;
        //RemoveCoroutines();
        //UndoScale();
        shouldUnscale = true;
        if (eventData.pointerCurrentRaycast.gameObject == gameObject
            || (eventData.position - eventData.pressPosition).magnitude <= 9)
            Pressed();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //if (!clickable) return;
        //print("Click " + Time.time);
        //RemoveCoroutines();
        //DoScale();
        //Invoke("UndoScale", time * peak);
        //Pressed();
    }

    private void OnEnable()
    {
        originalScale = transform.localScale;
    }

    private void OnDisable()
    {
        RemoveCoroutines();
    }

    void DoScale()
    {
        scaling = ScaleEnumerator(originalScale, originalScale * scale, time * peak, true);
        StartCoroutine(scaling);
    }

    void UndoScale()
    {
        unscaling = ScaleEnumerator(transform.localScale, originalScale, time * (1 - peak), false);
        StartCoroutine(unscaling);
    }

    IEnumerator ScaleEnumerator(Vector3 from, Vector3 to, float time, bool doScale)
    {
        float t0 = 0;
        while (t0 < time)
        {
            yield return null;
            t0 += Time.deltaTime;
            Vector3 scale = Vector3.Lerp(from, to, t0 / time);
            gameObject.transform.localScale = scale;
        }
        if (doScale)
        {
            yield return new WaitUntil(() => shouldUnscale);
            UndoScale();
        }
    }

    void Pressed()
    {
        SoundManager.Instance.PlaySound(SoundName.Button);
        if (onClick != null) onClick.Invoke();
    }

    void RemoveCoroutines()
    {
        if (scaling != null)
        {
            StopCoroutine(scaling);
            scaling = null;
        }
        if (unscaling != null)
        {
            StopCoroutine(unscaling);
            unscaling = null;
        }
        transform.localScale = originalScale;
    }
}