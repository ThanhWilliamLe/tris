﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    public Component target;

    public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);
    public float time = 1;
    public float delay = 0;
    public bool hideObjOnDone;

    float ranTime;
    float oriAlpha;

    private void Awake()
    {
        if (HasAlpha()) oriAlpha = ObjectUtils.GetColor(target).a;
    }

    private void OnEnable()
    {
        if (!HasAlpha())
        {
            print("Fader failed. Disabling... " + this.name);
            enabled = false;
            return;
        }

        delay = Mathf.Abs(delay);
        ranTime = -delay;
        SetAlpha();
    }

    private void Update()
    {
        if (ranTime >= 0)
        {
            if (ranTime >= time)
            {
                if (hideObjOnDone) gameObject.SetActive(false);
                else enabled = false;
                return;
            }
            SetAlpha();
        }
        ranTime = Mathf.Clamp(ranTime + Time.deltaTime, -delay, time);
    }

    bool HasAlpha()
    {
        return ObjectUtils.HasColor(target);
    }

    void SetAlpha()
    {
        Color c = ObjectUtils.GetColor(target);
        c.a = curve.Evaluate(time != 0 ? ranTime / time : 0) * oriAlpha;
        ObjectUtils.SetColor(target, c);
    }

}
