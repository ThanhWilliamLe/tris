﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AutoSize : MonoBehaviour
{
    public RectTransform rectAffect;
    public RectTransform rect;
    public Vector2 padding;

    void Update()
    {
        if (rect != null && rectAffect != null)
        {
            float w = rect.rect.width + padding.x * 2;
            float h = rect.rect.height + padding.y * 2;

            rectAffect.sizeDelta = new Vector2(w, h);
        }
    }
}
