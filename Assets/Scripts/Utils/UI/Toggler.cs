﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class Toggler : Button
{
    public bool toggled;
    public Component target;
    public Sprite onSprite, offSprite;
    public UnityEvent onToggleChanged;

    protected override void Start()
    {
        ObjectUtils.SetSprite(target, toggled ? onSprite : offSprite);
    }

    public void SetToggled(bool b)
    {
        toggled = b;
        ObjectUtils.SetSprite(target, toggled ? onSprite : offSprite);
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);
        if (interactable)
        {
            toggled = !toggled;
            ObjectUtils.SetSprite(target, toggled ? onSprite : offSprite);
            ObjectUtils.SetMonoEnabled<Pulser>(gameObject, true);
            onToggleChanged.Invoke();
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Toggler))]
public class TogglerCustomEditor : Editor
{
    [MenuItem("GameObject/WillMenu/Simple Toggle", false, 100)]
    static void CreateSimpleToggler(MenuCommand menuCommand)
    {
        GameObject go = new GameObject("Simple toggle");
        go.AddComponent<Toggler>();
        go.AddComponent<Pulser>();
        go.AddComponent<Image>();

        GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
        Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
        Selection.activeObject = go;
    }

    public override void OnInspectorGUI()
    {
        Toggler t = target as Toggler;
        t.interactable = EditorGUILayout.Toggle("Interactable", t.interactable);
        t.toggled = EditorGUILayout.Toggle("Toggled", t.toggled);
        t.target = (Component)EditorGUILayout.ObjectField("Target", t.target, typeof(Component), true);
        t.onSprite = (Sprite)EditorGUILayout.ObjectField("On sprite", t.onSprite, typeof(Sprite), true, GUILayout.Height(16));
        t.offSprite = (Sprite)EditorGUILayout.ObjectField("Off sprite", t.offSprite, typeof(Sprite), true, GUILayout.Height(16));

        this.serializedObject.Update();
        EditorGUILayout.PropertyField(this.serializedObject.FindProperty("onToggleChanged"), true);
        this.serializedObject.ApplyModifiedProperties();
    }
}
#endif