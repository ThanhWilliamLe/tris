﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISettings : MonoBehaviour
{
    public InputField nameInput;
    public Toggler soundToggler;
    public ScaleButton noAds;

    private void Start()
    {
        nameInput.text = GameData.Name;
        soundToggler.SetToggled(GameData.Sound);
        noAds.gameObject.SetActive(GameData.Ads);
    }

    public void OnSoundToggled(Toggler toggler)
    {
        GameData.Sound = toggler.toggled;
    }

    public void OnNameInput(InputField s)
    {
        GameData.Name = s.text;
    }

    public void OnNoAdsPressed()
    {

    }
}
