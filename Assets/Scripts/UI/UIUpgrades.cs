﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUpgrades : MonoBehaviour
{
    [Space]
    [Header("UI")]
    public Text coinText;

    [Space]
    [Header("Power upgrade")]
    public GameObject powersGroup;

    [Space]
    [Header("Colors")]
    public Color levelUnlocked;
    public Color levelLocked;
    public Color upgradableBtn;
    public Color nonUpgradableBtn;

    [Space]
    [Header("Prefab")]
    public GameObject powerPrefab;
    public Tagger powerNameTag;
    public Tagger powerLevelGroupTag;
    public Tagger powerLevelIconTag;
    public Tagger powerBtnTag;
    public Tagger powerBtnTextTag;

    public List<UIUpgradePowerEntry> powerEntries;

    bool isInitialized;

    private void OnEnable()
    {
        if (!isInitialized)
        {
            ObjectUtils.ClearChildrens(powersGroup.transform);
            if (powerEntries == null) powerEntries = new List<UIUpgradePowerEntry>();
            else powerEntries.Clear();

            InitPowerEntries();
            isInitialized = true;
        }
        UpdateAllPowerEntries();
        UpdateCoinText();
        GameManager.Instance.AddEventListener(GameEvent.UpgradeChanged, OnUpgradeChanged);
        GameManager.Instance.AddEventListener(GameEvent.CoinChanged, OnCoinChanged);
    }

    private void OnDisable()
    {
        GameManager.Instance.RemoveEventListener(GameEvent.UpgradeChanged, OnUpgradeChanged);
        GameManager.Instance.RemoveEventListener(GameEvent.CoinChanged, OnCoinChanged);
    }

    void InitPowerEntries()
    {
        foreach (UpgradePower power in Upgrades.Instance.powers)
        {
            GameObject entryObject = Instantiate(powerPrefab, powersGroup.transform);

            Text name = Tagger.FindFirstWithTag<Text>(powerNameTag.objTag, entryObject);
            name.text = power.display;

            Transform levelGroup = Tagger.FindFirstWithTag<Transform>(powerLevelGroupTag.objTag, entryObject);
            GameObject levelIco = Tagger.FindFirstWithTag<Image>(powerLevelIconTag.objTag, entryObject).gameObject;
            for (int i = 1; i < power.MaxLevel; i++)
            {
                Instantiate(levelIco, levelGroup);
            }
            Image[] lvlIcons = Tagger.FindAllWithTag<Image>(powerLevelIconTag.objTag, levelGroup.gameObject);

            ScaleButton button = Tagger.FindFirstWithTag<ScaleButton>(powerBtnTag.objTag, entryObject);
            TextSetter buttonText = Tagger.FindFirstWithTag<TextSetter>(powerBtnTextTag.objTag, entryObject);
            button.onClick.AddListener(() =>
            {
                if (GameData.UpgradePowerLevel(power))
                    SoundManager.Instance.PlaySound(SoundName.Button_Upgrade);
            });

            UIUpgradePowerEntry uiupe = new UIUpgradePowerEntry
            {
                power = power,
                levels = new List<Image>(lvlIcons),
                button = button,
                buttonText = buttonText
            };

            powerEntries.Add(uiupe);
        }
    }

    void UpdateAllPowerEntries()
    {
        foreach (UIUpgradePowerEntry e in powerEntries) UpdatePowerEntry(e);
    }

    void UpdatePowerEntry(UIUpgradePowerEntry e)
    {
        if (e == null) return;
        int level = GameData.GetUpgradedPowerLevel(e.power);

        for (int i = 0; i < e.levels.Count; i++)
        {
            bool on = i < level;
            e.levels[i].color = on ? levelUnlocked : levelLocked;
        }

        UpgradeCostAndValue cav = null;
        if (level >= 0 && level < e.power.MaxLevel)
            cav = e.power.costsAndValues[level];

        e.button.gameObject.SetActive(cav != null);
        bool upgradable = cav != null && cav.cost <= GameData.Coin;
        e.button.clickable = upgradable;
        e.button.gameObject.GetComponent<Image>().color = upgradable ? upgradableBtn : nonUpgradableBtn;
        e.buttonText.Apply(cav != null ? cav.cost : 0);
    }

    void OnUpgradeChanged(GameEvent e, object data)
    {
        UpgradePower p = data as UpgradePower;
        if (p != null)
        {
            UIUpgradePowerEntry entry = powerEntries.Find((ent) => ent.power.Equals(p));
            if (entry != null)
            {
                UpdatePowerEntry(entry);
                return;
            }
        }
        UpdateAllPowerEntries();
    }

    void OnCoinChanged(GameEvent e, object data)
    {
        UpdateCoinText();
        UpdateAllPowerEntries();
    }

    void UpdateCoinText()
    {
        coinText.text = GameData.Coin.ToString();
        ObjectUtils.SetMonoEnabled<Pulser>(coinText.gameObject, true);
    }
}

[System.Serializable]
public class UIUpgradePowerEntry
{
    public UpgradePower power;
    public List<Image> levels;
    public ScaleButton button;
    public TextSetter buttonText;
}
