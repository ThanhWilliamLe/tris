﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UIGameRewardVideo : MonoBehaviour
{
    public TextSetter rewardText;
    public Color coinTextColor = Color.white;
    public Color hpTextColor = Color.white;
    public Color hpCallTextColor = Color.white;
    [Range(0, 1)]
    public float lowHPRatio = 0.25f;
    [Range(0, 1)]
    public float midHPRatio = 0.5f;
    public float coinMult = 1;
    public int minCoin = 10;

    [TextArea(3, 6)]
    public string midHPCall;
    [TextArea(3, 6)]
    public string lowHPCall;

    float hpHeal = 0;
    int coinGive = 0;

    private void OnEnable()
    {
        float hpRatio = GameManager.CurrentPlayer.HP / GameManager.CurrentPlayer.MaxHP;

        float hpHealRatio = hpRatio <= lowHPRatio ? 0.5f : hpRatio <= midHPRatio ? 0.25f : 0;
        hpHeal = GameManager.CurrentPlayer.MaxHP * hpHealRatio;

        float coinGiveRatio = 1 - Mathf.Pow(hpHealRatio, 2.5f);
        coinGive = (int)Mathf.Max(minCoin,
            GameManager.CurrentPlayer.ScoreToCoin(GameManager.CurrentPlayer.Score)
            * coinMult * coinGiveRatio
            );

        List<string> s0l = new List<string>
        {
            "<color=#",ColorUtility.ToHtmlStringRGB(coinTextColor),">",
            (coinGive*GameData.GetUpgradePowerValue(UpgradePowerType.Coin,1)).ToString()," coins</color>"
        };
        if (hpHealRatio > 0)
        {
            s0l.Add(" and <color=#");
            s0l.Add(ColorUtility.ToHtmlStringRGB(hpTextColor));
            s0l.Add(">");
            s0l.Add(Mathf.RoundToInt(hpHealRatio * 100).ToString());
            s0l.Add("% hp</color>");
        }
        string s0 = string.Join("", s0l.ToArray());

        string s1 = "";
        if (hpRatio <= midHPRatio)
        {
            string s1t = hpRatio <= lowHPRatio ? lowHPCall : midHPCall;
            s1 = string.Join("", new[] {
                "<color=#", ColorUtility.ToHtmlStringRGB(hpCallTextColor),">",s1t,"</color>"
            });
        }

        rewardText.Apply(s0, s1);
    }

    public void WatchRewardVideo()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo", new ShowOptions() { resultCallback = WatchRewardVideoResult });
        }
        else
        {
            gameObject.SetActive(false);
            GameManager.CurrentGame.GameState = GameState.Play;
        }
    }

    void WatchRewardVideoResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            GameManager.CurrentPlayer.HP += hpHeal;
            GameManager.CurrentGame.AddAndSpawnCoins(GameManager.CurrentPlayer.transform.position, coinGive, 30, 1f, 1f);
        }
        GameManager.CurrentGame.GameState = GameState.Play;
    }
}

