﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UIPlayerStats : MonoBehaviour
{
    public RectTransform hpValue;
    public Text scoreValue;
    public GameObject bulletShootButton;
    public Image bulletShootOverlay;
    public GameObject missileShootButton;
    public Image missileShootOverlay;

    public void OnEnable()
    {
        GameManager.Instance.AddEventListener(GameEvent.HPChanged, OnHPChanged);
        GameManager.Instance.AddEventListener(GameEvent.ScoreChanged, OnScoreChanged);
        GameManager.Instance.AddEventListener(GameEvent.BulletCountChanged, OnBulletCountChanged);
        GameManager.Instance.AddEventListener(GameEvent.MissleCountChanged, OnMissleCountChanged);
    }

    public void OnDisable()
    {
        GameManager.Instance.RemoveEventListener(GameEvent.HPChanged, OnHPChanged);
        GameManager.Instance.RemoveEventListener(GameEvent.ScoreChanged, OnScoreChanged);
        GameManager.Instance.RemoveEventListener(GameEvent.BulletCountChanged, OnBulletCountChanged);
        GameManager.Instance.RemoveEventListener(GameEvent.MissleCountChanged, OnMissleCountChanged);
    }

    void OnHPChanged(GameEvent e, object data)
    {
        if (data is float && hpValue != null)
        {
            float f = Mathf.Clamp01((float)data);
            hpValue.anchorMax = new Vector2(f, hpValue.anchorMax.y);
        }
    }

    void OnScoreChanged(GameEvent e, object data)
    {
        if (data is float && scoreValue != null)
        {
            float f = (float)data;

            int prevScore = 0;
            int.TryParse(scoreValue.text, out prevScore);
            int newScore = Mathf.FloorToInt(f);

            scoreValue.text = newScore.ToString();
            if (newScore != prevScore) ObjectUtils.SetMonoEnabled<Pulser>(scoreValue.gameObject, true);
        }
    }

    void OnBulletCountChanged(GameEvent e, object data)
    {
        int[] bullets = data as int[];
        bulletShootOverlay.fillAmount = (float)bullets[0] / bullets[1];
        bulletShootButton.SetActive(bullets[0] > 0);
    }

    void OnMissleCountChanged(GameEvent e, object data)
    {
        int[] missiles = data as int[];
        missileShootOverlay.fillAmount = (float)missiles[0] / missiles[1];
        missileShootButton.SetActive(missiles[0] > 0);
    }
}
