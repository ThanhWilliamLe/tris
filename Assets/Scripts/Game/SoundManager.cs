﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Sound Manager", menuName = "Game/Sound Manager")]
public class SoundManager : ScriptableObject
{
    private static SoundManager instance;
    public static SoundManager Instance
    {
        get { return instance = instance ?? Resources.Load<SoundManager>("Sound Manager"); }
    }

    public AudioSource currentSource;
    public AudioMixerGroup masterAudio;
    public List<SoundEntry> sounds;

    private void OnEnable()
    {
        SetSound(GameEvent.SettingsChanged, null);
        GameManager.Instance.AddEventListener(GameEvent.SettingsChanged, SetSound);
    }

    private void OnDestroy()
    {
        GameManager.Instance.RemoveEventListener(GameEvent.SettingsChanged, SetSound);
    }

    void SetSound(GameEvent e, object data)
    {
        masterAudio.audioMixer.SetFloat("Volume", GameData.Sound ? 0 : -80);
    }

    public void PopulateSoundList()
    {
        List<SoundName> unadded = new List<SoundName>(System.Enum.GetValues(typeof(SoundName)).Cast<SoundName>());
        unadded.RemoveAll((a) => sounds.Find((e) => e.name.Equals(a)) != null);
        unadded.ForEach((a) => sounds.Add(new SoundEntry() { name = a }));
        sounds.Sort((a, b) => a.name.CompareTo(b.name));
    }

    public void PlaySound(SoundName name, float volumeScale = 1)
    {
        if (currentSource == null) return;

        AudioClip ac = GetAudioClip(name);
        if (ac == null) return;

        currentSource.PlayOneShot(ac, volumeScale);
    }

    public void PlayBGM(SoundName name)
    {
        if (currentSource == null) return;

        AudioClip ac = GetAudioClip(name);
        if (ac == null) return;

        currentSource.clip = ac;
        currentSource.Play();
    }

    public void BGMSpeed(float f = 1)
    {
        if (currentSource == null) return;
        currentSource.pitch = f;
    }

    public void PauseBGM()
    {
        if (currentSource == null) return;
        currentSource.Pause();
    }

    public void UnpauseBGM()
    {
        if (currentSource == null) return;
        currentSource.UnPause();
    }

    public AudioClip GetAudioClip(SoundName name)
    {
        SoundEntry se = sounds.Find((e) => e.name.Equals(name));
        return se == null ? null : se.clip;
    }
}

[System.Serializable]
public class SoundEntry
{
    public SoundName name;
    public AudioClip clip;
}

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(SoundEntry))]
public class SoundEntryDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        EditorGUI.MultiPropertyField(position,
            new[] { new GUIContent(""), new GUIContent("") },
            property.FindPropertyRelative("name"));
        EditorGUI.EndProperty();
    }
}

[CustomEditor(typeof(SoundManager))]
public class SoundManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        SoundManager t = target as SoundManager;
        serializedObject.UpdateIfRequiredOrScript();

        t.masterAudio = (AudioMixerGroup)EditorGUILayout.ObjectField("Master audio",
            t.masterAudio, typeof(AudioMixerGroup), false);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("sounds"), true);
        if (GUILayout.Button("Populate")) t.PopulateSoundList();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif

public enum SoundName
{
    BG_Home, BG_Game,
    Button, Button_Upgrade,
    Coin_Spawn,
    Fire_Bullet, Fire_Missile,
    Hit_Player, Hit_Enemy,
    Power_ForceField, Power_Buff, Power_Eat,
    Game_Over, Game_Highscore,
}