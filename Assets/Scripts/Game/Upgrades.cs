﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(fileName = "Upgrades", menuName = "Game/Upgrades")]
public class Upgrades : ScriptableObject
{
    private static Upgrades instance;
    public static Upgrades Instance
    {
        get { return instance = instance ?? Resources.Load<Upgrades>("Upgrades"); }
    }

    public List<UpgradeSkin> skins;
    public List<UpgradePower> powers;

    public UpgradePower UpgradePowerByType(UpgradePowerType type)
    {
        return powers.Find((p) => p.type == type);
    }
}

[System.Serializable]
public class UpgradeSkin
{
    public string name = "skin name";
    public Sprite sprite;
    public int cost = 1;
}

[System.Serializable]
public class UpgradePower
{
    public string display;
    public UpgradePowerType type;
    public List<UpgradeCostAndValue> costsAndValues;

    public int MaxLevel { get { return costsAndValues == null ? 0 : costsAndValues.Count; } }
    public string PrefString { get { return GamePrefConst.UPGRADE_PREFIX + type; } }
}

public enum UpgradePowerType
{
    Power, Speed, Health, Coin, ForceSize, BuffTime
}

[System.Serializable]
public class UpgradeCostAndValue
{
    public int cost;
    public float value;
}


#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(UpgradeSkin))]
public class UpgradeSkinDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        Rect posName = new Rect(position.x, position.y, position.width / 3, position.height);
        Rect posSprite = new Rect(position.x + position.width / 3, position.y, position.width / 3, position.height);
        Rect posColor = new Rect(position.x + position.width / 3 * 2, position.y, position.width / 3, position.height);

        EditorGUI.PropertyField(posName, property.FindPropertyRelative("name"), GUIContent.none, true);
        EditorGUI.PropertyField(posSprite, property.FindPropertyRelative("sprite"), GUIContent.none, true);
        EditorGUI.PropertyField(posColor, property.FindPropertyRelative("cost"), GUIContent.none, true);

        EditorGUI.EndProperty();
    }
}

[CustomPropertyDrawer(typeof(UpgradeCostAndValue))]
public class UpgradeCostAndValueDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        EditorGUI.MultiPropertyField(EditorGUI.IndentedRect(position),
            new[] { new GUIContent("C"), new GUIContent("V") },
            property.FindPropertyRelative("cost"));

        EditorGUI.EndProperty();
    }
}
#endif
