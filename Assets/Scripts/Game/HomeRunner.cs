﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeRunner : MonoBehaviour
{
    public GameObject homeUITransition;
    public AudioSource homeAudioSource;
    AsyncOperation gameSceneLoad;

    private void Start()
    {
        gameSceneLoad = SceneManager.LoadSceneAsync("GameScene");
        gameSceneLoad.allowSceneActivation = false;
        SoundManager.Instance.currentSource = homeAudioSource;
        SoundManager.Instance.PlayBGM(SoundName.BG_Home);
    }

    public void TransitionToPlay()
    {
        homeUITransition.SetActive(true);
        StartCoroutine(Transitioning());
    }

    IEnumerator Transitioning()
    {
        Fader fader = homeUITransition.GetComponent<Fader>();
        yield return new WaitUntil(() => !fader.enabled);
        PlayGame();
    }

    public void PlayGame()
    {
        gameSceneLoad.allowSceneActivation = true;
    }

}
