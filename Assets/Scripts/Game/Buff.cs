﻿using System;
using System.Collections.Generic;
using System.Reflection;

[Serializable]
public class Buff
{
    public string name;
    public int type;
    public float value;
    public float timeLeft;

    public Buff(int t, float v, float time)
    {
        name = "ONLY AVAILABLE IN EDITOR";
#if UNITY_EDITOR
        name = BuffType.NameOf(t);
#endif
        type = t;
        value = v;
        timeLeft = time;
    }

    public bool IsType(int t)
    {
        return (t & type) != 0;
    }
}

public static class BuffType
{
    public const int PlayerSpeed = 1 << 0;
    public const int PlayerDamage = 1 << 1;
    public const int ForceFieldSize = 1 << 2;
    public const int EnemySlow = 1 << 3;

    public const int All = PlayerSpeed | PlayerDamage | ForceFieldSize | EnemySlow;

    public static int[] AllArray = new int[] { PlayerSpeed, PlayerDamage, ForceFieldSize, EnemySlow };

    public static string NameOf(int t)
    {
        List<string> strings = new List<string>();
        FieldInfo[] fields = typeof(BuffType).GetFields(BindingFlags.Static | BindingFlags.Public);
        foreach (FieldInfo f in fields)
        {
            if (f.Name.Equals("All")) continue;
            if (f.FieldType != typeof(int)) continue;

            int value = (int)f.GetRawConstantValue();
            if ((value & t) != 0)
            {
                strings.Add(f.Name);
            }
        }
        return string.Join(" | ", strings.ToArray());
    }
}