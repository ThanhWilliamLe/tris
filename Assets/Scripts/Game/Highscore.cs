﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.Build;
#endif

[CreateAssetMenu(fileName = "Highscore", menuName = "Game/Highscore")]
public class Highscore : ScriptableObject
{
    private Highscore() { }
    private static Highscore instance;
    public static Highscore Instance
    {
        get { return instance = instance ?? Resources.Load<Highscore>("Highscore"); }
    }

    [SerializeField]
    List<HighscoreEntry> highscores;

    public bool AddScore(long score)
    {
        HighscoreEntry entry = new HighscoreEntry
        {
            identifier = SystemInfo.deviceUniqueIdentifier,
            name = GameData.Name,
            score = score,
            time = System.DateTime.Now.Ticks
        };
        highscores.Add(entry);

        highscores.Sort((a, b) =>
        {
            if (b.score == a.score) return a.time.CompareTo(b.time);
            return b.score.CompareTo(a.score);
        });
        return highscores.IndexOf(entry) == 0;
    }

    public long GetTopScore()
    {
        return highscores[0].score;
    }

    public void Clear()
    {
        highscores.Clear();
    }
}

[System.Serializable]
public class HighscoreEntry
{
    public string identifier;
    public string name;
    public long score;
    public long time;
    public bool uploaded;
}

#if UNITY_EDITOR
class HighscoreBuildProcessor : IPreprocessBuildWithReport
{
    int IOrderedCallback.callbackOrder
    {
        get
        {
            return 0;
        }
    }

    public void OnPreprocessBuild(BuildReport report)
    {
        Highscore.Instance.Clear();
    }
}
#endif