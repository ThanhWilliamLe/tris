﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameData
{
    public static string Name
    {
        get { return PlayerPrefs.GetString(GamePrefConst.SETTING_NAME, ""); }
        set
        {
            PlayerPrefs.SetString(GamePrefConst.SETTING_NAME, value); PlayerPrefs.Save();
            GameManager.Instance.InvokeEvent(GameEvent.SettingsChanged, "name");
        }
    }

    public static bool Sound
    {
        get { return PlayerPrefs.GetInt(GamePrefConst.SETTING_SOUND, 1) == 1; }
        set
        {
            PlayerPrefs.SetInt(GamePrefConst.SETTING_SOUND, value ? 1 : 0); PlayerPrefs.Save();
            GameManager.Instance.InvokeEvent(GameEvent.SettingsChanged, "sound");
        }
    }

    public static bool Ads
    {
        get { return PlayerPrefs.GetInt(GamePrefConst.SETTING_ADS, 1) == 1; }
        set
        {
            PlayerPrefs.SetInt(GamePrefConst.SETTING_ADS, value ? 1 : 0); PlayerPrefs.Save();
            GameManager.Instance.InvokeEvent(GameEvent.SettingsChanged, "ads");
        }
    }

    public static int ReplaysWithoutAds
    {
        get { return PlayerPrefs.GetInt(GamePrefConst.REPLAYS_WITHOUT_ADS, 0); }
        set { PlayerPrefs.SetInt(GamePrefConst.REPLAYS_WITHOUT_ADS, value); PlayerPrefs.Save(); }
    }

    public static int Coin
    {
        get { return PlayerPrefs.GetInt(GamePrefConst.COIN); }
        set
        {
            bool notify = Coin != value;
            PlayerPrefs.SetInt(GamePrefConst.COIN, Mathf.Max(0, value));
            PlayerPrefs.Save();
            if (notify) GameManager.Instance.InvokeEvent(GameEvent.CoinChanged, Coin);
        }
    }

    public static int GetUpgradedPowerLevel(UpgradePower power)
    {
        return Mathf.Clamp(PlayerPrefs.GetInt(power.PrefString), 0, power.MaxLevel);
    }

    public static void SetUpgradedPowerLevel(UpgradePower power, int level)
    {
        level = Mathf.Clamp(level, 0, power.MaxLevel);
        PlayerPrefs.SetInt(power.PrefString, level);
        PlayerPrefs.Save();
        GameManager.Instance.InvokeEvent(GameEvent.UpgradeChanged, power);
    }

    public static bool UpgradePowerLevel(UpgradePower power, bool withCoin = true)
    {
        int nextLevel = GetUpgradedPowerLevel(power) + 1;
        if (!withCoin)
        {
            SetUpgradedPowerLevel(power, nextLevel);
            return true;
        }
        else
        {
            UpgradeCostAndValue cav = null;
            if (nextLevel <= power.MaxLevel) cav = power.costsAndValues[nextLevel - 1];
            if (cav != null && Coin >= cav.cost)
            {
                Coin -= cav.cost;
                SetUpgradedPowerLevel(power, nextLevel);
                return true;
            }
            else return false;
        }
    }

    public static float GetUpgradePowerValue(UpgradePowerType type, float def)
    {
        UpgradePower power = Upgrades.Instance.UpgradePowerByType(type);
        if (power != null && GetUpgradedPowerLevel(power) > 0)
        {
            UpgradeCostAndValue cav = power.costsAndValues[GetUpgradedPowerLevel(power) - 1];
            return cav.value;
        }
        return def;
    }
}

#if UNITY_EDITOR
public class GameDataWindow : EditorWindow
{
    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Game/GameData editor")]
    static void Init()
    {
        GetWindow<GameDataWindow>().Show();
    }

    private void OnEnable()
    {
        autoRepaintOnSceneChange = true;
    }

    private void OnDisable()
    {
        autoRepaintOnSceneChange = false;
    }

    private void OnGUI()
    {
        GameData.Coin = EditorGUILayout.IntField("Coin", GameData.Coin);
    }
}
#endif