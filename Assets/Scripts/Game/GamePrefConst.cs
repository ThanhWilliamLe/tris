﻿public static class GamePrefConst
{
    public const string SETTING_NAME = "tris.setting.name";
    public const string SETTING_SOUND = "tris.setting.sound";
    public const string SETTING_ADS = "tris.setting.ads";

    public const string REPLAYS_WITHOUT_ADS = "tris.rwa";

    public const string COIN = "tris.data.coin";
    public const string UPGRADE_PREFIX = "tris.data.u_";
}