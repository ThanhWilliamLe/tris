﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlActionButtons : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public ActionButtonType type;
    public float delay = 0.2f;

    public float effectScale = 1.15f;
    public float effectTime = 0.25f;

    Coroutine runningEffect;
    Vector3 oriScale;
    bool holding;
    float wait;

    private void OnEnable()
    {
        oriScale = transform.localScale;
        holding = false;
        wait = 0;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        holding = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        holding = false;
    }

    private void Update()
    {
        if (wait > 0) wait = Mathf.Max(0, wait - Time.deltaTime);
        if (holding && wait <= 0)
        {
            if (runningEffect != null) StopCoroutine(runningEffect);
            runningEffect = StartCoroutine(ShootButtonEffect());

            Shoot();
            wait = delay;
        }
    }

    IEnumerator ShootButtonEffect()
    {
        float peak = 0.3f;
        float passedTime = 0;
        while (passedTime < effectTime)
        {
            float ratio = Mathf.Clamp01(passedTime / effectTime);
            if (ratio < peak) ratio /= peak;
            else ratio = (1 - ratio) / (1 - peak);

            gameObject.transform.localScale = Vector3.Lerp(oriScale, oriScale * effectScale, ratio);

            passedTime += Time.deltaTime;
            yield return null;
        }
        runningEffect = null;
    }

    void Shoot()
    {
        switch (type)
        {
            case ActionButtonType.Bullet:
                ShootBullet();
                break;
            case ActionButtonType.Missile:
                ShootMissile();
                break;
            default:
                break;
        }
    }

    void ShootBullet()
    {
        Player player = GameManager.CurrentPlayer;
        if (player.BulletCount > 0) player.ShootBullet();
    }

    void ShootMissile()
    {
        Player player = GameManager.CurrentPlayer;
        if (player.MissileCount > 0) player.ShootMissile();
    }
}

public enum ActionButtonType
{
    Bullet,
    Missile,
}
