﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager
{
    private GameManager() { }
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null) instance = new GameManager();
            return instance;
        }
    }
    public static GameRunner CurrentGame { get { return Instance.currentGameRunner; } }
    public static Player CurrentPlayer { get { return Instance.currentGameRunner.player; } }

    public GameRunner currentGameRunner;
    public Dictionary<GameEvent, EventListener> eventListeners = new Dictionary<GameEvent, EventListener>();
    public delegate void EventListener(GameEvent e, object data);

    public void AddEventListener(GameEvent e, EventListener action)
    {
        if (!eventListeners.ContainsKey(e))
        {
            EventListener ee = delegate { };
            eventListeners.Add(e, ee);
        }
        eventListeners[e] -= action;
        eventListeners[e] += action;
    }

    public void RemoveEventListener(GameEvent e, EventListener action)
    {
        if (eventListeners.ContainsKey(e))
        {
            eventListeners[e] -= action;
        }
    }

    public void InvokeEvent(GameEvent e, object data)
    {
        if (eventListeners.ContainsKey(e))
        {
            eventListeners[e].Invoke(e, data);
        }
    }
}

public enum GameEvent
{
    SettingsChanged,
    GameStateChanged,
    HPChanged,
    ScoreChanged,
    CoinChanged,
    BulletCountChanged,
    MissleCountChanged,
    ShieldChanged,
    UpgradeChanged,
}