﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.Advertisements;

public class GameUIRunner : MonoBehaviour
{
    public List<GameStateUI> stateUIList = new List<GameStateUI>();

    [Space]
    [Header("Play")]
    public Text coinText;
    public ControlJoystick joystick;

    [Space]
    [Header("Pause")]
    public GameObject pauseButton;
    public GameObject unpauseGuide;
    public GameObject watchVideoAsk;

    [Space]
    [Header("Game over")]
    public Counter gameOverScoreCounter;
    public Text gameOverCoinText;
    public Text gameOverTitle;
    public Text highscoreText;
    public GameObject newHighscoreUI;
    public string[] gameOverTexts = new[] { "GAME OVER", "YOU DIED!", "TRY AGAIN?", "YOU DID PRETTY WELL" };

    public void StartInitState()
    {
        SetUIsForState(GameState.Init);
        pauseButton.SetActive(false);
        unpauseGuide.SetActive(true);
        coinText.text = GameData.Coin.ToString();
    }

    public void StartPlayState()
    {
        SetUIsForState(GameState.Play);
    }

    public void StartPauseState()
    {
        SetUIsForState(GameState.Pause);
        pauseButton.SetActive(true);
        unpauseGuide.SetActive(false);
        watchVideoAsk.SetActive(false);
    }

    public void StartEndState(bool highscore)
    {
        SetUIsForState(GameState.End);

        SoundManager.Instance.PlaySound(highscore ? SoundName.Game_Highscore : SoundName.Game_Over);
        gameOverTitle.text = gameOverTexts.GetRandom();
        gameOverCoinText.text = GameData.Coin.ToString();
        highscoreText.text = Highscore.Instance.GetTopScore().ToString();

        if (!highscore)
        {
            PostNewHighScoreScreen();
        }
        else
        {
            newHighscoreUI.SetActive(true);
            Invoke("PostNewHighScoreScreen", 2f);
        }
    }

    void PostNewHighScoreScreen()
    {
        float animTime = 2f;
        gameOverScoreCounter.StartCounter(GameManager.CurrentPlayer.Score, 0, animTime);
        GameManager.CurrentGame.AddAndSpawnCoins(new Vector3(0, GameManager.CurrentGame.bounds.Max.y, 0),
            GameManager.CurrentPlayer.ScoreToCoin(GameManager.CurrentPlayer.Score), 40, 0.35f, animTime);
        newHighscoreUI.SetActive(false);
    }

    void SetUIsForState(GameState state)
    {
        Dictionary<GameObject, bool> shouldEnable = new Dictionary<GameObject, bool>();
        Dictionary<GraphicRaycaster, bool> shouldRaycast = new Dictionary<GraphicRaycaster, bool>();
        foreach (GameStateUI gsu in stateUIList)
        {
            bool should = gsu.state == state;
            foreach (GameObject gui in gsu.ui)
            {
                if (!shouldEnable.ContainsKey(gui)) shouldEnable.Add(gui, should);
                shouldEnable[gui] |= should;
            }
            foreach (GraphicRaycaster ray in gsu.ray)
            {
                if (!shouldRaycast.ContainsKey(ray)) shouldRaycast.Add(ray, should);
                shouldRaycast[ray] |= should;
            }
        }

        foreach (KeyValuePair<GameObject, bool> kvp in shouldEnable)
        {
            if (kvp.Key.activeSelf && !kvp.Value) kvp.Key.SetActive(false);
            else if (!kvp.Key.activeSelf && kvp.Value) kvp.Key.SetActive(true);
        }

        foreach (KeyValuePair<GraphicRaycaster, bool> kvp in shouldRaycast)
        {
            if (kvp.Key.enabled && !kvp.Value) kvp.Key.enabled = false;
            else if (!kvp.Key.enabled && kvp.Value) kvp.Key.enabled = true;
        }

    }

    public void OnReplayBtnPressed()
    {
        int cons = GameData.ReplaysWithoutAds;
        cons++;

        if (GameData.Ads && cons >= 2 && Advertisement.IsReady("replayvideo"))
        {
            Advertisement.Show("replayvideo", new ShowOptions()
            {
                resultCallback = (a) => { ActuallyReplay(); }
            });
            cons = 0;
        }
        else ActuallyReplay();
        GameData.ReplaysWithoutAds = cons;
    }

    void ActuallyReplay()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void OnHomeBtnPressed()
    {
        SceneManager.LoadScene("HomeScene");
    }

    public void AskWatchVideo()
    {
        watchVideoAsk.SetActive(true);
    }
}

[System.Serializable]
public class GameStateUI
{
    public GameState state;
    public GameObject[] ui;
    public GraphicRaycaster[] ray;
}