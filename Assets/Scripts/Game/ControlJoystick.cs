﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlJoystick : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler
{
    public float length = 128;
    public RectTransform stick;

    public KeyCode up = KeyCode.W;
    public KeyCode down = KeyCode.S;
    public KeyCode left = KeyCode.A;
    public KeyCode right = KeyCode.D;

    bool dragging;
    Vector2 zero = Vector2.zero;
    Vector2 stickOri = new Vector2(0.5f, 0.5f);
    Vector2? scalePointer;
    Vector2 stickDir;

    private void OnEnable()
    {
        scalePointer = null;
    }

    private void UpdateScalePointer()
    {
        if (scalePointer == null)
        {
            float a = Screen.currentResolution.width;
            float b = Screen.currentResolution.height;
            float c = Screen.width;
            float d = Screen.width;

            RectTransform canvas = (RectTransform)GetComponentInParent<Canvas>().transform;
            scalePointer = canvas.rect.size;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        dragging = true;
        TrySetGameModePlay();
        Drag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Drag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        ResetJoystick(false);
        TrySetGameModePaused();
        dragging = false;
    }

    public void Drag(PointerEventData e)
    {
        UpdateScalePointer();

        Vector2 localE = e.position;
        localE.x = localE.x / Screen.width * scalePointer.GetValueOrDefault().x;
        localE.y = localE.y / Screen.height * scalePointer.GetValueOrDefault().y;

        stickDir = (localE - ((RectTransform)transform).anchoredPosition) / length;
        stickDir = Vector2.ClampMagnitude(stickDir, 1);

        stick.anchorMin = stick.anchorMax = stickOri + stickDir / 2;
    }

    private void Update()
    {
        if (!dragging)
        {
            if (Input.GetKeyDown(up) || Input.GetKeyDown(down) || Input.GetKeyDown(right) || Input.GetKeyDown(left))
            {
                TrySetGameModePlay();
            }
            float keyboardVert = Input.GetKey(up) ? 1 : Input.GetKey(down) ? -1 : 0;
            float keyboardHorz = Input.GetKey(right) ? 1 : Input.GetKey(left) ? -1 : 0;
            if (keyboardVert != 0 || keyboardHorz != 0)
            {
                stickDir = new Vector2(keyboardHorz, keyboardVert).normalized;
                stick.anchorMin = stick.anchorMax = stickOri + stickDir / 2;
            }
            else if (Input.GetKeyUp(up) || Input.GetKeyUp(down) || Input.GetKeyUp(right) || Input.GetKeyUp(left))
            {
                ResetJoystick(false);
                TrySetGameModePaused();
            }
        }

        if (GameManager.CurrentGame.GameState != GameState.Play)
        {
            ResetJoystick(true);
        }

        if (stickDir.x != 0 || stickDir.y != 0)
        {
            SetPlayerMovement(Time.deltaTime, false);
        }
    }

    void TrySetGameModePlay()
    {
        return;
        if (GameManager.CurrentGame.GameState == GameState.Init ||
            GameManager.CurrentGame.GameState == GameState.Pause)
        {
            GameManager.CurrentGame.GameState = GameState.Play;
        }
    }

    void TrySetGameModePaused()
    {
        return;
        if (GameManager.CurrentGame.GameState == GameState.Play)
        {
            GameManager.CurrentGame.GameState = GameState.Pause;
        }
    }

    public void ResetJoystick(bool force)
    {
        stickDir = zero;
        stick.anchorMin = stick.anchorMax = stickOri;
        SetPlayerMovement(0, force);
    }

    public void SetPlayerMovement(float timePassed, bool force)
    {
        if (force || GameManager.CurrentGame.GameState == GameState.Play)
        {
            GameManager.CurrentPlayer.SetMovement(stickDir, timePassed);
        }
    }
}
