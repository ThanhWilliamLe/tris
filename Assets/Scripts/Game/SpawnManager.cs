﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnManager
{
    GameRunner gameRunner;

    [Range(0, 1)]
    public float disabledPowerSpawnerTimeScale = 0.25f;

    public List<Spawner> enemySpawners;
    public List<Spawner> powerSpawners;

    public HashSet<Enemy> spawnedEnemies;
    public HashSet<Power> spawnedPowers;

    //public HashSet<Enemy> enemiesToRemove;
    //public HashSet<Power> powersToRemove;

    public SpawnManager(GameRunner gr)
    {
        gameRunner = gr;

        Vector3 enemyPoolPos = new Vector3(100, 100, 0);
        enemySpawners = gameRunner.enemySpawnerSO.ToSpawnerList(enemyPoolPos);
        spawnedEnemies = new HashSet<Enemy>();
        //enemiesToRemove = new HashSet<Enemy>();

        Vector3 powerPoolPos = new Vector3(100, -100, 0);
        powerSpawners = gameRunner.powerSpawnerSO.ToSpawnerList(powerPoolPos);
        spawnedPowers = new HashSet<Power>();
        //powersToRemove = new HashSet<Power>();
    }

    public void RemoveAllDead()
    {
        //spawnedEnemies.ExceptWith(enemiesToRemove);
        //enemiesToRemove.Clear();

        //spawnedPowers.ExceptWith(powersToRemove);
        //powersToRemove.Clear();

        spawnedEnemies.RemoveWhere((e) => e == null || e.dead || e.gameObject == null);
        spawnedPowers.RemoveWhere((p) => p == null || p.eaten || p.gameObject == null);
    }

    public void UpdateSpawners(float timePassed)
    {
        foreach (Spawner s in enemySpawners)
        {
            if (!s.enabled) continue;
            s.spawnDelay -= timePassed;
            if (s.spawnDelay <= 0)
            {
                int spawnCount = Mathf.RoundToInt(Mathf.Lerp(s.settings.countRange.x, s.settings.countRange.y, Random.value));
                spawnCount = Mathf.CeilToInt(spawnCount * Mathf.Pow(gameRunner.gameDifficulty, 0.5f));
                for (int i = 0; i < spawnCount; i++) SpawnAnEnemy(s);
                s.spawnDelay = Random.Range(s.settings.interval.x, s.settings.interval.y);
            }
        }
        foreach (Spawner s in powerSpawners)
        {
            s.spawnDelay -= timePassed * (s.enabled ? 1 : disabledPowerSpawnerTimeScale);
            if (s.spawnDelay <= 0 && s.enabled)
            {
                int spawnCount = Mathf.RoundToInt(Mathf.Lerp(s.settings.countRange.x, s.settings.countRange.y, Random.value));
                spawnCount = Mathf.CeilToInt(spawnCount * Mathf.Pow(gameRunner.gameDifficulty, 0.35f));
                for (int i = 0; i < spawnCount; i++) SpawnAPower(s);
                s.enabled = false;
                s.spawnDelay = Random.Range(s.settings.interval.x, s.settings.interval.y);
            }
        }
    }

    void SpawnAPower(Spawner s)
    {
        Vector2 v1 = gameRunner.bounds.Min;
        Vector2 v2 = gameRunner.bounds.Max;
        float edge = 1f;
        float x = Mathf.Lerp(v1.x + edge, v2.x - edge, Random.value);
        float y = Mathf.Lerp(v1.y + edge, v2.y - edge, Random.value);

        GameObject prefab = s.settings.prefabs.GetRandom();
        GameObject newPower = GameObject.Instantiate(prefab, gameRunner.powersGroup);
        newPower.transform.localPosition = new Vector3(x, y, 0);
        Power power = newPower.GetComponent<Power>();
        power.spawner = s;
        power.Spawned();

        spawnedPowers.Add(power);
    }

    public void PowerDestroyed(Power power)
    {
        if (power != null && power.spawner != null)
        {
            Spawner spawner = power.spawner;
            bool shouldEnableSpawner = true;
            foreach (Power p in spawnedPowers)
            {
                if (p != power && p != null && p.spawner != null && p.spawner == spawner)
                {
                    shouldEnableSpawner = false;
                    break;
                }
            }
            if (shouldEnableSpawner) spawner.enabled = true;
        }

        //powersToRemove.Add(power);
        //spawnedPowers.Remove(power);
    }

    void SpawnAnEnemy(Spawner s)
    {
        float screenY = gameRunner.cam.orthographicSize;
        float screenX = gameRunner.cam.orthographicSize * gameRunner.cam.aspect;
        float spawnDist = Mathf.Sqrt(screenX * screenX + screenY * screenY) + 1;
        float spawnDistRange = 3f;

        if (s.pool.Count == 0)
        {
            GameObject prefab = s.settings.prefabs.GetRandom();
            GameObject newEnemy = GameObject.Instantiate(prefab, gameRunner.enemiesGroup);
            Enemy newEnemyScript = newEnemy.GetComponent<Enemy>();
            newEnemyScript.spawner = s;
            PoolEnemy(newEnemyScript);
        }

        GameObject enemy = s.pool.Dequeue();
        enemy.transform.position =
            Quaternion.Euler(0, 0, Random.value * 360f)
            * Vector3.up
            * (spawnDist + Random.value * spawnDistRange);

        Enemy e = enemy.GetComponent<Enemy>();
        e.OnSpawned();
        spawnedEnemies.Add(e);
    }

    public void PoolEnemy(Enemy e)
    {
        Spawner s = e.spawner;
        if (s != null)
        {
            //if (spawnedEnemies.Contains(e)) enemiesToRemove.Add(e);
            //spawnedEnemies.Remove(e);
            e.dead = true;

            e.gameObject.transform.position = s.poolPosition;
            s.pool.Enqueue(e.gameObject);
            if (s.settings.poolSize > 0)
            {
                while (s.pool.Count > s.settings.poolSize)
                {
                    GameObject.Destroy(s.pool.Dequeue());
                }
            }
        }
        else
        {
            GameObject.Destroy(e.gameObject);
        }
    }
}