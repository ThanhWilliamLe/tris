﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ObjectFly : MonoBehaviour
{
    const float THRESHOLD = 0.1f;
    const float THRESHOLD_SCREEN = 100f;

    public Vector3 targetPos;
    public float speed = 1;
    [Range(0, 1)]
    public float speedRate = 1;
    public bool fixedSpeed = true;
    public bool autoDie = false;
    public bool local = true;
    public bool screen = false;
    public Action callBack;

    Vector3 prevVelo;

    private void Update()
    {
        Vector3 oriDiff = targetPos - (local ? transform.localPosition : transform.position);
        Vector3 diff = oriDiff;
        if (fixedSpeed) diff = diff.normalized * speed;
        else diff = Vector3.ClampMagnitude(diff, speed);

        Vector3 velo = Vector3.Lerp(prevVelo, diff, speedRate);
        prevVelo = velo;
        velo *= Time.deltaTime;

        if (oriDiff.sqrMagnitude < (screen ? THRESHOLD_SCREEN : THRESHOLD))
        {
            if (callBack != null) callBack.Invoke();
            if (autoDie) Destroy(gameObject);
        }
        else
        {
            if (local) transform.localPosition = transform.localPosition + velo;
            else transform.position = transform.position + velo;
        }
    }
}