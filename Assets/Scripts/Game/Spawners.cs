﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Spawners", menuName = "Game/Spawners")]
public class Spawners : ScriptableObject
{
    public SpawnerSettings[] settings;

    public List<Spawner> ToSpawnerList(Vector3 poolPosition)
    {
        return new List<Spawner>(settings.Select(setting => new Spawner(setting, poolPosition)));
    }
}

[Serializable]
public class SpawnerSettings
{
    public int poolSize = 100;
    public bool initEnable = true;
    public Vector2 interval = Vector2.one;
    public Vector2 initDelay = Vector2.zero;
    public Vector2Int countRange = new Vector2Int(1, 1);
    public WeightedSpawnList prefabs;
}

[Serializable]
public class Spawner
{
    public SpawnerSettings settings;
    public bool enabled;
    public float spawnDelay = 0;
    public Vector3 poolPosition = new Vector2(100, 100);
    public Queue<GameObject> pool = new Queue<GameObject>();

    public Spawner(SpawnerSettings s, Vector3 poolPos)
    {
        settings = s;
        enabled = s.initEnable;
        poolPosition = poolPos;
        spawnDelay = UnityEngine.Random.Range(s.initDelay.x, s.initDelay.y);
    }
}

[Serializable]
public class WeightedSpawnEntry : WeightedEntry<GameObject>
{ }

[Serializable]
public class WeightedSpawnList : WeightedList<WeightedSpawnEntry, GameObject>
{ }