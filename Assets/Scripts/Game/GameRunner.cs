﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameRunner : MonoBehaviour
{
    [Header("Statics")]
    public Camera cam;
    public GameBounds bounds;
    public Player player;
    public GameUIRunner gameUIRunner;
    public AudioSource gameAudioSource;

    [Space]
    [Header("Effects")]
    public GameObject coinPrefab;
    public Transform coinsGroup;

    [Space]
    [Header("Spawns")]
    public SpawnManager spawnManager;
    public Spawners enemySpawnerSO;
    public Spawners powerSpawnerSO;
    public Transform enemiesGroup;
    public Transform powersGroup;
    public Transform playerBulletsGroup;

    [Space]
    [Header("Game live")]
    public float gamePlayTime;
    public float gameDifficulty;
    public float timeMult;
    public List<Buff> runningBuffs = new List<Buff>();

    [SerializeField]
    private GameState gameState;
    public GameState GameState
    {
        get { return gameState; }
        set
        {
            if (value == gameState) return;

            gameState = value;
            if (gameState == GameState.Init) StartInitState();
            else if (gameState == GameState.Play) StartPlayState();
            else if (gameState == GameState.Pause) StartPauseState();
            else if (gameState == GameState.End) StartEndState();
        }
    }

    private void Start()
    {
        GameState = GameState.Init;
        spawnManager = new SpawnManager(this);
        gameDifficulty = 1;
        timeMult = 1;
    }

    private void OnEnable()
    {
        GameManager.Instance.currentGameRunner = this;
        SoundManager.Instance.currentSource = gameAudioSource;
    }

    private void OnDestroy()
    {
        GameManager.Instance.currentGameRunner = null;
        SoundManager.Instance.currentSource = null;
    }

    public void SetState(GameState state)
    {
        GameState = state;
    }

    void StartInitState()
    {
        gameUIRunner.StartInitState();

        GameTime.time = 0;
        GameTime.deltaTime = 0;
        GameTime.fixedDeltaTime = 0;

        SoundManager.Instance.PlayBGM(SoundName.BG_Game);
        GameState = GameState.Play;
    }

    void StartPlayState()
    {
        SoundManager.Instance.UnpauseBGM();
        gameUIRunner.StartPlayState();
    }

    void StartPauseState()
    {
        SoundManager.Instance.PauseBGM();
        gameUIRunner.StartPauseState();
    }

    void StartEndState()
    {
        SoundManager.Instance.BGMSpeed(1);
        SoundManager.Instance.PauseBGM();
        bool hs = Highscore.Instance.AddScore((long)player.Score);
        gameUIRunner.StartEndState(hs);
    }

    private void Update()
    {
        if (gameState == GameState.Play)
        {
            float timePassed = Time.deltaTime * timeMult;

            gamePlayTime += timePassed;
            GameTime.time = gamePlayTime;
            GameTime.deltaTime = timePassed;

            gameDifficulty += timePassed * 0.05f;

            player.Score += timePassed;
            player.UpdatePlayer(timePassed);
            player.playerShooting.Update(timePassed);

            spawnManager.UpdateSpawners(timePassed);
            spawnManager.RemoveAllDead();
            runningBuffs.RemoveAll((b) => b.timeLeft <= 0);
            foreach (Enemy e in spawnManager.spawnedEnemies)
            {
                e.EnemyUpdate(timePassed);
            }
            foreach (Power p in spawnManager.spawnedPowers)
            {
                p.PowerUpdate(timePassed);
            }
            foreach (Buff b in runningBuffs)
            {
                b.timeLeft -= timePassed;
            }

            if (player.HP <= 0) GameState = GameState.End;
        }
    }

    private void FixedUpdate()
    {
        if (gameState == GameState.Play)
        {
            float timePassed = Time.fixedDeltaTime * timeMult;
            GameTime.fixedDeltaTime = timePassed;

            player.FixedUpdatePlayer(timePassed);
            player.playerShooting.FixedUpdate(timePassed);

            spawnManager.RemoveAllDead();
            foreach (Enemy e in spawnManager.spawnedEnemies)
            {
                e.EnemyFixedUpdate(timePassed);
            }
        }
    }

    public void SetTimeMult(float f)
    {
        timeMult = f;
        SoundManager.Instance.BGMSpeed(Mathf.Pow(f, 0.3f));
    }

    public void AddBuff(Buff buff, bool stackable = true)
    {
        SoundManager.Instance.PlaySound(SoundName.Power_Buff);
        buff.timeLeft *= GameData.GetUpgradePowerValue(UpgradePowerType.BuffTime, 1);
        List<Buff> buffs = new List<Buff>();
        foreach (int buffType in BuffType.AllArray)
        {
            if (buff.IsType(buffType))
            {
                bool addable = true;
                if (!stackable)
                {
                    foreach (Buff rBuff in runningBuffs)
                    {
                        if (rBuff.IsType(buffType))
                        {
                            rBuff.value = Mathf.Max(rBuff.value, buff.value);
                            rBuff.timeLeft = Mathf.Max(rBuff.timeLeft, buff.timeLeft);
                            addable = false;
                            break;
                        }
                    }
                }
                if (addable) runningBuffs.Add(new Buff(buffType, buff.value, buff.timeLeft));
            }
        }
    }

    public float GetMultipliedBuffValue(int type, bool zeroAble = false)
    {
        float value = 1;
        foreach (Buff b in runningBuffs)
        {
            if (b.IsType(type))
            {
                value *= b.value;
            }
        }
        if (value == 0 && !zeroAble) return 0.000001f;
        return value;
    }

    public Transform FindClosest(Vector3 pos, EntityType type, float radius = -1, bool excludeDead = true)
    {
        float radiusSqr = radius * radius;
        if (type == EntityType.Player)
        {
            if (radius < 0 || (pos - player.transform.position).sqrMagnitude <= radiusSqr) return player.transform;
        }
        else if (type == EntityType.Enemy)
        {
            Transform closest = null;
            float distSqr = -1;
            foreach (Enemy e in spawnManager.spawnedEnemies)
            {
                if (e.dead && excludeDead) continue;
                float thisDistSqr = (e.transform.position - pos).sqrMagnitude;
                if ((radius < 0 || thisDistSqr < radiusSqr) && (closest == null || thisDistSqr < distSqr))
                {
                    distSqr = thisDistSqr;
                    closest = e.transform;
                }
            }
            return closest;
        }
        else if (type == EntityType.Power)
        {
            Transform closest = null;
            float distSqr = -1;
            foreach (Power p in spawnManager.spawnedPowers)
            {
                if (p.eaten && excludeDead) continue;
                float thisDistSqr = (p.transform.position - pos).sqrMagnitude;
                if ((radius < 0 || thisDistSqr < radiusSqr) && (closest == null || thisDistSqr < distSqr))
                {
                    distSqr = thisDistSqr;
                    closest = p.transform;
                }
            }
            return closest;
        }
        return null;
    }

    public void PauseGame(bool pause)
    {
        if (pause && GameState == GameState.Play) GameState = GameState.Pause;
        else if (!pause && GameState == GameState.Pause) GameState = GameState.Play;
    }

    public void SetGodMode(Toggle t)
    {
        player.god = t.isOn;
    }

    public void AddCoin(int coin)
    {
        if (coin <= 0) return;

        coin = (int)(coin * GameData.GetUpgradePowerValue(UpgradePowerType.Coin, 1));
        GameData.Coin += coin;
    }

    public void AddAndSpawnCoins(Vector3 oriPos, int coin, int maxSpawn, float randomness, float spawnTime)
    {
        if (coin <= 0) return;

        coin = (int)(coin * GameData.GetUpgradePowerValue(UpgradePowerType.Coin, 1));
        GameData.Coin += coin;

        int toSpawn = Mathf.Min(maxSpawn, coin);
        int addedCoinToUI = 0;

        Vector3 target = bounds.Max;
        for (int i = 0; i < toSpawn; i++)
        {
            int add = Mathf.FloorToInt((float)coin / toSpawn);
            if (i == toSpawn - 1) add = coin - addedCoinToUI;
            addedCoinToUI += add;

            Vector3 pos = oriPos + Random.insideUnitSphere * randomness;
            pos.z = oriPos.z;
            float speed = (target - pos).magnitude * Random.Range(0.8f, 1.2f);

            GameManager.CurrentGame.StartCoroutine(
                SpawnCoins(coinPrefab, coinsGroup, pos, target, speed, spawnTime / toSpawn * i, add)
                );
        }
    }

    IEnumerator SpawnCoins(GameObject prefab, Transform parent, Vector3 spawnPos, Vector3 target, float speed, float delay, int coinToAdd)
    {
        float timePassed = 0;
        while (timePassed < delay)
        {
            yield return null;
            timePassed += Time.deltaTime;
        }

        SoundManager.Instance.PlaySound(SoundName.Coin_Spawn, 0.5f);
        GameObject newCoin = Instantiate(prefab, parent);
        ObjectFly coinFly = newCoin.GetComponent<ObjectFly>();
        if (coinFly == null)
        {
            Destroy(newCoin);
            yield break;
        }
        else
        {
            newCoin.transform.position = spawnPos;
            coinFly.targetPos = target;
            coinFly.speed = speed;
            coinFly.callBack = () =>
            {
                Text coinText = GameState == GameState.End ?
                    GameManager.CurrentGame.gameUIRunner.gameOverCoinText :
                    GameManager.CurrentGame.gameUIRunner.coinText;

                int coinValue = 0;
                int.TryParse(coinText.text, out coinValue);
                coinValue += coinToAdd;

                coinText.text = coinValue.ToString();
                ObjectUtils.SetMonoEnabled<Pulser>(coinText.gameObject, true);
                SoundManager.Instance.PlaySound(SoundName.Coin_Spawn, 0.3f);
            };
        }
    }
}

public static class GameTime
{
    public static float deltaTime;
    public static float fixedDeltaTime;
    public static float time;
}

public enum GameState
{
    None, Init, Play, Pause, End
}

public enum EntityType
{
    Player, Enemy, Power, PlayerBullet, EnemyBullet
}