﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class GameBounds : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private float boundWidthBase = 0.005f;
    [SerializeField] private float boundInsetPixels = 50;
    [SerializeField] private float prevOrthoSize = -1;
    [SerializeField] private EdgeCollider2D col;
    [SerializeField] private LineRenderer lr;
    [SerializeField] private RectTransform uiCanvas;

    public Vector2 Min
    {
        get;
        private set;
    }
    public Vector2 Max
    {
        get;
        private set;
    }
    public Rect Rect
    {
        get
        {
            return new Rect(Min, Max - Min);
        }
    }
    public Vector3[] Corners
    {
        get
        {
            Vector2 v1 = Min;
            Vector2 v2 = Max;
            return new Vector3[] { v1, new Vector2(v1.x, v2.y), v2, new Vector2(v2.x, v1.y) };
        }
    }
    public Line[] Edges
    {
        get
        {
            Vector3[] c = Corners;
            return new Line[]
            {
                new Line(c[0],c[1]-c[0]),
                new Line(c[1],c[2]-c[1]),
                new Line(c[2],c[3]-c[2]),
                new Line(c[3],c[0]-c[3])
            };
        }
    }

    private void OnEnable()
    {
        prevOrthoSize = cam.orthographicSize - 1;
    }

    [ContextMenu("Update Bounds")]
    void ForceUpdate()
    {
        prevOrthoSize = cam.orthographicSize - 1;
        UpdateBounds();
    }

    [ExecuteInEditMode]
    public void LateUpdate()
    {
        UpdateBounds();
    }

    void UpdateBounds()
    {
        if (cam.orthographicSize != prevOrthoSize)
        {
            float ySizeRatio = 1 - 2 * boundInsetPixels / uiCanvas.rect.height;
            float xSizeRatio = 1 - 2 * boundInsetPixels / uiCanvas.rect.width;
            float ySize = cam.orthographicSize * ySizeRatio;
            float xSize = cam.orthographicSize * cam.aspect * xSizeRatio;
            Vector2[] vs = new Vector2[]
            {
                new Vector2(-xSize,-ySize),
                new Vector2(-xSize, ySize),
                new Vector2(xSize,ySize),
                new Vector2(xSize,-ySize),
                new Vector2(-xSize,-ySize)
            };
            col.points = vs;

            Min = new Vector2(-xSize, -ySize);
            Max = new Vector2(xSize, ySize);

            float lineWidth = boundWidthBase * cam.orthographicSize;
            Vector3[] v3s = new Vector3[]
            {
                new Vector3(-xSize - lineWidth/2, -ySize - lineWidth/2),
                new Vector3(-xSize - lineWidth/2, ySize + lineWidth/2),
                new Vector3(xSize + lineWidth/2, ySize + lineWidth/2),
                new Vector3(xSize + lineWidth/2, -ySize - lineWidth/2),
                new Vector3(-xSize - lineWidth, -ySize - lineWidth/2)
            };
            lr.widthMultiplier = lineWidth;
            lr.positionCount = vs.Length;
            lr.SetPositions(v3s);

            prevOrthoSize = cam.orthographicSize;
        }
    }
}
